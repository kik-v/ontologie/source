# input: https://dataportal.cbs.nl/detail/CBS/85877NED
import csv
import rdflib 
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, OWL
onzorg = Namespace('http://purl.org/ozo/onz-org#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
g = Graph()
g.bind('onz-org', onzorg)
g.bind('onz-g', onzg)
zk_regios = []
zk_regio_labels = []

# bron gebaseerd op: https://www.cbs.nl/nl-nl/cijfers/detail/85755NED
data_file = '/Users/marc/Downloads/85755NED-202403200000/Observations.csv'

with open(data_file, "r") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=";")
    header = next(csv_reader)  # Skip the first row
    for row in csv_reader:
        measure = row[header.index('Measure')]
        string_value = row[header.index('StringValue')].rstrip()
        gemeente_code = row[header.index('RegioS')]
        if measure == 'ZK0001': #Rij bevat een zorgkantoorcode
            zk_regio = URIRef('http://purl.org/ozo/onz-org/' + string_value)
            gemeente = URIRef('https://identifier.overheid.nl/tooi/id/gemeente/' + gemeente_code.lower())
            g.add((gemeente, onzg.partOf, zk_regio))
g.add((onzg.partOf, RDF.type, OWL.ObjectProperty))

g.serialize(destination='Ontologie_data/zkregios.ttl', format='turtle')