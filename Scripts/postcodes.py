# input: https://dataportal.cbs.nl/detail/CBS/85877NED
import csv
import rdflib 
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, OWL
onzorg = Namespace('http://purl.org/ozo/onz-org#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
g = Graph()
g.bind('onz-org', onzorg)
g.bind('onz-g', onzg)

gemeenten = []
provincies = []

# Zie https://www.cbs.nl/nl-nl/maatwerk/2023/35/buurt-wijk-en-gemeente-2023-voor-postcode-huisnummer voor de oorspronkelijke publicatie.
data_file = '/Users/marc/Downloads/Postcode_woonplaats.csv'

with open(data_file, "r") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=",")
    header = next(csv_reader)  # Skip the first row
    for row in csv_reader:
        postcode_waarde = row[header.index('postcode')]
        woonplaats = URIRef(row[header.index('woonplaats')])
        postcode_gebied = URIRef('http://purl.org/ozo/onz-org/postcodeGebied/' + postcode_waarde)
        postcode_identifier = URIRef('http://purl.org/ozo/onz-org/postcodeIdentifier/' + postcode_waarde)
        g.add((postcode_gebied, RDF.type, onzg.PostcodeArea))
        g.add((postcode_identifier, RDF.type, onzorg.Postcode))
        g.add((postcode_gebied, onzg.partOf, woonplaats))
        g.add((postcode_gebied, onzg.identifiedBy, postcode_identifier))
        g.add((postcode_identifier, onzg.hasDataValue, Literal(postcode_waarde)))

            
g.add((onzg.partOf, RDF.type, OWL.ObjectProperty))
g.add((onzg.identifiedBy, RDF.type, OWL.ObjectProperty))

g.serialize(destination='Ontologie_data/postcodes.ttl', format='turtle')