# Validate:
# 1. Sparql parsing after inerting # infront lines containing $ for parameters
# 2. Classes and preficates used in sparql queries on existence in ontology(ies)
# Uses config file for frequent use

import sys, traceback
from rdflib import Graph, URIRef
import pathlib as pl
import os
import yaml
import pandas as pd

from rdflib.plugins.sparql.parser import parseQuery
from rdflib.plugins.sparql.parserutils import prettify_parsetree

def get_ontologies(config) -> Graph:
    ontology = Graph()
    for file in os.listdir(config['ontologyfolder']):
        if not file.endswith('.owl'):
            continue
        if config['debug']:
            print('Included ontology: ' + file)
        ontology += Graph().parse(pl.Path(config['ontologyfolder']) / pl.Path(file))
    return ontology

def insert_comment_lines_with_parameter(query_txt) -> str:
    lines = query_txt.split('\n')
    _result = []
    for line in lines:
        if '$' in line:
            _result.append('#' + line)
        else:
            _result.append(line)
    result = '\n'.join(_result)

    return result

def get_classes_from_queries(config) -> list:
    query_classes, pnames = [], []
    for folder in config['sparqlfolders']:
        for sparql in os.listdir(folder):
            if not sparql.endswith('.rq'):
                continue
            with open(pl.Path(folder) / pl.Path(sparql), 'r') as file:
                _query_txt = file.read()
            # Insert # as comment in front of line with $ indicating a parameter
            # e.g.    BIND ($(vestiging) AS ?vestiging) 
            query_txt = insert_comment_lines_with_parameter(_query_txt)
            try:
                parsed_query = parseQuery(query_txt)
                ppt = prettify_parsetree(parsed_query)
                ppt_lines = ppt.split('\n')
                pname_lines, prefix_lines = [], []
                for line, ix in zip(ppt_lines, range(len(ppt_lines))):
                    if 'pname' in line:
                        pname_lines.append(ix)
                    if 'PrefixDecl' in line:
                        prefix_lines.append(ix)

                for ix in pname_lines:
                    prefix = ppt_lines[ix+2].split("'")[1]
                    if prefix in config['prefixes']:
                        localname = ppt_lines[ix+4].split("'")[1]
                        uri = URIRef(config['prefixes'][prefix] + localname)
                        #pnames.append(f"{prefix}:{localname}" )
                        pnames.append(uri)
            except:
                if config['debug']:
                    traceback.print_exc(file=sys.stdout)
                print('Error parsing ' + folder + ' / ' + sparql)
                if config['debug']:
                    print(query_txt)

    return list(set(pnames))


def get_classes_from_ontology(ontology) -> list:
    '''Create validation result string'''
    ontology.serialize('ontologie_debug.ttl', format='turtle')
    _classes = ontology.query("""select distinct ?clss where {?clss a owl:Class}""" )
    _data_properties = ontology.query("""select distinct ?prop where {?prop a owl:ObjectProperty}""" )
    _datatype_properties = ontology.query("""select distinct ?prop where {?prop a owl:DatatypeProperty}""" )
    _individuals = ontology.query("""select distinct ?ind where {?ind a owl:NamedIndividual}""" )
    classes = list([e[0] for e in _classes])
    dataproperties = list([e[0] for e  in _data_properties])
    datatype_properties = list([e[0] for e  in _datatype_properties])
    individuals = list([e[0] for e  in _individuals])
    if config['debug']:
        ind_str = "\n".join([i for i in individuals])
        ind_file = open('individuals_debug.txt' , 'w')
        ind_file.write(ind_str)
    return classes + dataproperties + datatype_properties + individuals

def check_classes(classes_from_queries, classes_from_ontology, config) -> str:
    validation_result = "Classes and predicates not in ontology:"
    validation_result_data = []
    # Is a class missing?
    faulty_classes = set(classes_from_queries) - set(classes_from_ontology)
    if config['debug']: print('\nClasses not in ontologies\n', faulty_classes)
    if faulty_classes:
        query_dict = {}
        for folder in config['sparqlfolders']:
            for sparql in os.listdir(folder):
                if not sparql.endswith('.rq'):
                    continue
                with open(pl.Path(folder) / pl.Path(sparql), 'r') as file:
                    _query_txt = file.read()
                    query_dict[sparql] = dict()
                    query_dict[sparql]['txt'] = _query_txt
                    query_dict[sparql]['folder'] = folder

        faulty_classes_result = {}
        for clss in faulty_classes:
            # Find sparql in which this class is found
            for prefix in config['prefixes']:
                if config['prefixes'][prefix] in clss:
                    uri = f"{prefix}:{clss.split('#')[1]}"
                    faulty_classes_result[uri] = []

        for uri in faulty_classes_result:
            for sparql in query_dict:
                if uri in query_dict[sparql]['txt']:
                    faulty_classes_result[uri].append(sparql)
                    validation_result += f"\n{uri} used in {sparql} from {query_dict[sparql]['folder']} "
                    validation_result_data.append([uri, query_dict[sparql]['folder'], sparql])
    else:
        print('All used classes are defined in the ontology(ies).')
    return validation_result_data

def main(config): 
    classes_from_queries = get_classes_from_queries(config)
    print('\nLogging:')
    if config['debug']:
        print('Classes found in queries:')
        print([c for c in classes_from_queries])

    ontology = get_ontologies(config)
    classes_from_ontology = get_classes_from_ontology(ontology)
    validated = check_classes(classes_from_queries, classes_from_ontology, config)

    print('\nCLasses found in queries : ', len(classes_from_queries))
    print('CLasses found in ontology: ', len(classes_from_ontology))
    if validated:
        print('\nClasses used in sparql not defined in ontologies:')
        print(pd.DataFrame(data=validated, 
                columns=['Class', ' Folder', 'Indicator']).sort_values(by=['Indicator', 'Class']))
    else:
        print('OK. All classes in the specified namespaces exist in the ontologies.')

if __name__ == '__main__':
    with open('query-validatie-config.yaml', 'r') as f:
        config = yaml.safe_load(f)
    # TODO handle config reading errors in detail
    main(config) 
