import requests
import json
import rdflib
owl_repo = '27327602'
sparql_repo = '27289823'
onto = rdflib.Graph()

def get_owl_files(repository_id):
    ontology_project = 'https://gitlab.com/api/v4/projects/' + repository_id
    owl_files = []
    x = requests.get(ontology_project + '/repository/tree')
    res = json.loads(x.text)
    for item in res:
        name = item['name']
        if name[-3:] == 'owl':
            owl_files.append(name)
    return(owl_files)

def return_raw_file(repository_id, branch, filename):
    ontology_project = 'https://gitlab.com/api/v4/projects/' + repository_id
    x = requests.get(ontology_project + '/repository/files/' + filename + '/raw?ref=' + branch)
    return(x.content)

def get_onto_from_gitlab(branch):
    queries = get_owl_files(owl_repo)
    for query in queries:
        raw_file = return_raw_file (owl_repo, branch, query)
        onto.parse(raw_file, format='application/rdf+xml')
    return(onto)