import re
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import RDF, RDFS
from dataclasses import dataclass

# prefixes
onzorg = Namespace('http://purl.org/ozo/onz-org#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
data = Namespace('http://data.example.org/')

g = Graph()
g.bind('onz-org', onzorg)
g.bind('onz-g', onzg)
g.bind('data', data)

@dataclass
class Adres:
    straat: str
    huisnummer: str
    postcode: str
    plaats: str

    def __post_init__(self):
        self.uri = BNode()
        
    def to_rdf(self):
        g = Graph()
        nummeraanduiding = BNode()
        postcode = URIRef('http://purl.org/ozo/onz-org/postcodeIdentifier/' + self.postcode.replace(' ','').upper())

        g.add((self.uri, RDF.type, onzorg.Adres))
        g.add((self.uri, onzg.hasPart, nummeraanduiding))
        g.add((nummeraanduiding, RDF.type, onzorg.NummerAanduiding))
        g.add((nummeraanduiding, onzg.hasPart, postcode))
        g.add((self.uri, RDFS.label, Literal(self.straat + ' ' + self.huisnummer + ', ' + self.postcode + ' ' + self.plaats)))

        return g

@dataclass
class Vestiging:
    vest_nummer: str
    naam: str
    adres: Adres

    def __post_init__(self):
        self.uri = URIRef(data + 'vestiging/' + self.vest_nummer)

    def to_rdf(self):
        g = Graph()
        g.add((self.uri, RDF.type, onzorg.Vestiging))
        g.add((self.uri, RDFS.label, Literal(self.naam)))

        # vestigingsnummer
        vestigings_nr = BNode()
        g.add((vestigings_nr, RDF.type, onzorg.Vestigingsnummer))
        g.add((vestigings_nr, onzg.hasDataValue, Literal(self.vest_nummer)))
        g.add((self.uri, onzg.identifiedBy, vestigings_nr))

        # adres
        if self.adres:
            localizableArea = BNode()
            g.add((self.uri, onzg.hasLocalizableArea, localizableArea))
            g.add((localizableArea, RDF.type, onzg.LocalizableArea))
            g.add((localizableArea, onzg.identifiedBy, self.adres.uri))
        return g
        
@dataclass
class Organisatie:
    kvknummer: str
    naam: str
    sbi_codes: list

    def __post_init__(self):
        self.uri = URIRef(data + 'organisatie/' + self.kvknummer)
        self.vestigingen = []

    def add_vestiging(self, vestiging:Vestiging):
        self.vestigingen.append(vestiging)

    def to_rdf(self) -> Graph:
        g = Graph()

        # organisatie naam
        g.add((self.uri, RDFS.label, Literal(self.naam)))

        # sbi
        for sbi_code in self.sbi_codes:
            sbi_match = (re.search("\([\.0-9]*\)$",sbi_code).group()[1:-1])
            if sbi_match == '87.10' :
                g.add((self.uri, RDF.type, onzg.NursingHomeOrganization))
            elif sbi_match == '88.10.1' :
                g.add((self.uri, RDF.type, onzg.HomeCareOrganization))
            elif sbi_match == '87.30.2' :
                g.add((self.uri, RDF.type, onzg.ResidentialCareHome))
            else:
                g.add((self.uri, RDF.type, onzorg.Organisatie))
                print(f"Let op, {self.naam} lijkt geen verpleeghuisorganisatie te zijn")

        # kvk Nummer
        kvk_nummer = BNode()
        g.add((kvk_nummer, RDF.type, onzorg.KvkNummer))
        g.add((kvk_nummer, onzg.hasDataValue, Literal(self.kvknummer)))
        g.add((self.uri, onzg.identifiedBy, kvk_nummer))

        # vestigingen
        for vestiging in self.vestigingen:
            g.add((self.uri, onzorg.heeftVestiging, vestiging.uri))

        return g

