# Genereer RDF beschrijving van organisatie
Het script lrza_to_ttl.py genereert RDF data van een zorgorganisatie op basis van de gegevens die in het Landelijk Register Zorgaanbieders (LRZa) staan.

## Input
Geef het KvK-nummer als input van de organisatie. In het script staan drie voorbeelden van organisaties met hun KvK-nummer

## Gegevens
- KvK nummer (is ook input)
- Vestigingsnummers
- Namen van organisatie en vestigingen
- Adressen (alleen de postcode wordt als URI opgenomen. Adres alleen als label. Dit is gedaan om het script eenvoudig te houden. Zou in de toekomst uitgebreid kunnen worden naar het opnemen van URI's voor de woonplaats en eventueel de straat)

## Output
Turtle bestand met de naam van de organisatie als filenaam. De triples in het bestand komen overeen met de organisatie ontologie (http://purl.org/ozo/onz-org).

