import requests
import json
import re
from lrza_objects import *
from rdflib import Graph

# Voorbeeld KvK-nummers
# kvk_nummer = '09110174' #sensire
# kvk_nummer = '41227061' #heliomare
kvk_nummer = '41032142' #zorgvilla twente

g = Graph()

def get_info(kvk_nummer) -> dict:
    url = 'https://zoeken.zorgaanbiedersportaal.nl/api/organisatie/' + kvk_nummer
    try:
        # Make the GET request
        response = requests.get(url)

        # Check if the response is valid (HTTP status code 200)
        if response.status_code == 200:
            dict_response = response_to_dict(response.text)
            return (dict_response)  # Process the response as needed
        else:
            print(f'Error: Received response code {response.status_code}')
            # You can handle different status codes here if needed
    except requests.exceptions.RequestException as e:
        print(f'An error occurred: {e}')

def response_to_dict(response) -> dict:
    try:
        dict_obj = json.loads(response)
        return(dict_obj)  # Output the resulting dictionary
    except json.JSONDecodeError as e:
        print(f'Error: {e}')

# Haal organisatie-informatie op van LRZa o.b.v. KvK nummer
organisatie_info = get_info(kvk_nummer=kvk_nummer)

# Maak organisatie aan
organisatie = Organisatie(
    kvknummer = organisatie_info["kvknummer"], 
    naam = organisatie_info["naam"],
    sbi_codes = organisatie_info["sbicodes"]
)

# Voeg Vestigingen toe aan Organisatie
for vestiging_info in organisatie_info["vestigingen"]:
    # Maak adres aan
    adres = Adres(
        straat=vestiging_info["adres"]["straat"],
        huisnummer=vestiging_info["adres"]["huisnummer"],
        postcode=vestiging_info["adres"]["postcode"],
        plaats=vestiging_info["adres"]["plaats"]
    )
    g += adres.to_rdf()

    vestiging = Vestiging(
        vest_nummer=vestiging_info["vestigingsnummer"],
        naam=vestiging_info["naam"],
        adres=adres
    )
    g += vestiging.to_rdf()
    organisatie.add_vestiging(vestiging)

g += organisatie.to_rdf()

# Sla rdf op als turtle bestand
g.serialize(destination=organisatie_info["naam"] + '.ttl', format='turtle')
print("Informatie over " + organisatie_info["naam"] + " opgeslagen in \"" + organisatie_info["naam"] + ".ttl\"")