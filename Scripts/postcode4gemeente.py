import csv
import rdflib 
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, OWL
onzorg = Namespace('http://purl.org/ozo/onz-org#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
g = Graph()
g.bind('onz-org', onzorg)
g.bind('onz-g', onzg)

# Zie https://www.cbs.nl/nl-nl/maatwerk/2024/35/buurt-wijk-en-gemeente-2024-voor-postcode-huisnummer voor de oorspronkelijke publicatie en download.
data_file = 'pc6hnr20240801_gwb.csv'

with open(data_file, "r") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=",")
    header = next(csv_reader)  # Skip the first row
    postcode_waarde_oud = False
    for row in csv_reader:
        postcode_waarde = row[header.index('PC6')][:4] # gebruik alleen eerste vier cijfers aangezien deze uniek zijn per gemeente
        if postcode_waarde != postcode_waarde_oud:
            gemeente = URIRef('https://identifier.overheid.nl/tooi/id/gemeente/gm' + row[header.index('Gemeente2024')])
            postcode_gebied = URIRef('http://purl.org/ozo/onz-org/postcodeGebied/' + postcode_waarde)
            postcode_identifier = URIRef('http://purl.org/ozo/onz-org/postcodeIdentifier/' + postcode_waarde)
            g.add((postcode_gebied, RDF.type, onzg.PostcodeArea))
            g.add((postcode_identifier, RDF.type, onzorg.Postcode))
            g.add((postcode_gebied, onzg.partOf, gemeente))
            g.add((postcode_gebied, onzg.identifiedBy, postcode_identifier))
            g.add((postcode_identifier, onzg.hasDataValue, Literal(postcode_waarde)))
            postcode_waarde_oud = postcode_waarde
            print(postcode_waarde)

            
g.add((onzg.partOf, RDF.type, OWL.ObjectProperty))
g.add((onzg.identifiedBy, RDF.type, OWL.ObjectProperty))

g.serialize(destination='Ontologie_data/postcode-4_naar_gemeente.ttl', format='turtle')