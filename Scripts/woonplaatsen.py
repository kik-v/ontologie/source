# input: https://dataportal.cbs.nl/detail/CBS/85877NED
import csv
import rdflib 
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, OWL
onzorg = Namespace('http://purl.org/ozo/onz-org#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
g = Graph()
g.bind('onz-org', onzorg)
g.bind('onz-g', onzg)

gemeenten = []
provincies = []

# bron gebaseerd op: https://www.cbs.nl/nl-nl/cijfers/detail/85877NED
data_file = '85877NED-202403140000/Observations.csv'

with open(data_file, "r") as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=";")
    header = next(csv_reader)  # Skip the first row
    for row in csv_reader:
        measure = row[header.index('Measure')]
        string_value = row[header.index('StringValue')].rstrip()
        if measure == 'WP0001': #Rij bevat een nieuwe woonplaats
            # add triples
            woonplaats = URIRef('http://bag.basisregistraties.overheid.nl/bag/id/woonplaats/' + string_value[2:])
            identifier_woonplaats = BNode()
            g.add((woonplaats, RDF.type, onzorg.Woonplaats))
            g.add((identifier_woonplaats, RDF.type, onzorg.CBSIdentifier))
            g.add((identifier_woonplaats, onzg.hasDataValue, Literal(string_value)))
            g.add((woonplaats, onzg.identifiedBy, identifier_woonplaats))
        elif measure == 'GM000C': # Rij bevat naam van de Gemeente
            gemeente_naam = string_value
        elif measure == 'GM000B': # Rij bevat identifier van de Gemeente 
            gemeente = URIRef('https://identifier.overheid.nl/tooi/id/gemeente/' + string_value.lower())
            g.add((woonplaats, onzg.partOf, gemeente))
            if gemeente_naam not in gemeenten:
                gemeenten.append(gemeente_naam)
                identifier_gemeente = BNode()
                g.add((gemeente, RDFS.label, Literal(gemeente_naam, datatype = XSD.string)))
                g.add((gemeente, RDF.type, onzg.MunicipalityArea))
                g.add((identifier_gemeente, RDF.type, onzorg.CBSIdentifier))
                g.add((identifier_gemeente, onzg.hasDataValue, Literal(string_value)))
                g.add((gemeente, onzg.identifiedBy, identifier_gemeente))
        elif measure == 'PV0002': # Rij bevat naam van de Provincie
            provincie_naam = string_value
        elif measure == 'PV0001': # Rij bevat identifier voor Provincie
            provincie = URIRef('https://identifier.overheid.nl/tooi/id/provincie/' + string_value.lower())
            g.add((gemeente, onzg.partOf, provincie))
            if provincie_naam not in provincies:
                provincies.append(provincie_naam)
                provincie_identifier = BNode()
                g.add((provincie, RDF.type, onzg.ProvinceArea))
                g.add((provincie, RDFS.label, Literal(provincie_naam)))
                g.add((provincie_identifier, RDF.type, onzorg.CBSIdentifier))
                g.add((provincie_identifier, onzg.hasDataValue, Literal(string_value)))
                g.add((provincie, onzg.identifiedBy, provincie_identifier))
            
g.add((onzg.partOf, RDF.type, OWL.ObjectProperty))
g.add((onzg.identifiedBy, RDF.type, OWL.ObjectProperty))

g.serialize(destination='Ontologie_data/woonplaatsen.ttl', format='turtle')