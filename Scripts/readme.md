# Scripts

## Genereer_dagen_instanties
Maakt een .ttl aan in ../Ontologie data
In de .ttl zitten instanties voor alle kalenderdagen in 2020 t/m 2024. Deze krijgen een volgnummer, waardoor bij het berekenen van indicatoren het exacte aantal dagen in een periode kan worden vastgesteld.

## postcode4gemeente
Maakt postcode-4_naar_gemeente.ttl aan. Hierin staan alle vier cijfierige postcodes in Nederland (bron: CBS) en de link naar de bijbehorende gemeente (URI op basis van tooi ontologie: https://standaarden.overheid.nl/tooi/doc/tooi-ontologie/)

## xml2rgs
Neemt de officiele download van https://www.referentiegrootboekschema.nl/kbase en extraheert daaruit de juiste individuals om RGS te kunnen ondersteuenen. Resultaat is een .ttl bestand met die individuals.

## Sparql2Readme
Maak readme (en check sparql queries) van de concepten tbv publicatie van het uitwisselprofiel.
Dit script leest de gespecificeerde branch van Gitlab.

## Sparql query Validatie
Dit script controleert sparql syntax en of alle classes en predicates van de aangegeven namespaces 
in de ontologieen voorkomen.
Dit script verondersteld een lokaal geselcetreede branch voor de sparqls en ontologien.

Gebruik config.yaml voor het specificeren van de omgeving waarin dit script draait. Hierin is ook een debug mode.

Uit het voorbeeld hieronder blijkt:
1. syntax fouten in de queries te zitten, waardoor deze niet geparsed kunnen worden.
2. de namespace van de klasse FinancialEntity is fout

Bestanden met debug in de name en de yaml configuratie zijn geen onderdeel van de repository middels git ignore.

### Voorbeeld output
```
Error parsing E:/gitlab/sparql/Zorgkantoren/ / Indicator 13.3.rq
Error parsing E:/gitlab/sparql/Zorgkantoren/ / Indicator 13.4.rq
Error parsing E:/gitlab/sparql/VWS/ / Indicator 1.2.rq
Error parsing E:/gitlab/sparql/VWS/ / Indicator 4.3.rq
Error parsing E:/gitlab/sparql/VWS/ / Indicator 4.4.rq

Logging:
CLasses found in queries :  171
CLasses found in ontology:  976

   Class                    Folder                 Indicator
0  onz-fin:FinancialEntity  E:/gitlab/sparql/NZa/  Indicator 10.2.rq
1  onz-fin:FinancialEntity  E:/gitlab/sparql/NZa/  Indicator 10.3.rq
2  onz-fin:FinancialEntity  E:/gitlab/sparql/NZa/  Indicator 9.2.rq
3  onz-fin:FinancialEntity  E:/gitlab/sparql/NZa/  Indicator 9.3.rq
```
### Voorbeeld query-validatie-config.yaml
```
# Validatie config file in yaml format

# Debug mode , extra logging and temp file creations
debug : True   # True or False

# Input locations for .rq files
sparqlfolders    : [
  'E:/gitlab/sparql/IGJ/',
  'E:/gitlab/sparql/NZa/',
  'E:/gitlab/sparql/Zorgkantoren/',
  'E:/gitlab/sparql/VWS/',
  'E:/gitlab/sparql/Basisveiligheid/',
  'E:/gitlab/sparql/Personele samenstelling/',
  'E:/gitlab/sparql/Algemeen/',
  ]

# Input locations for .owl files
ontologyfolder  : 'E:/gitlab/source/'

# relevant prefixes
prefixes: {
  onz-g:    "http://purl.org/ozo/onz-g#",
  onz-fin:  "http://purl.org/ozo/onz-fin#",
  onz-pers: "http://purl.org/ozo/onz-pers#",
  onz-org:  "http://purl.org/ozo/onz-org#",
  onz-zorg: "http://purl.org/ozo/onz-zorg#",
}
```