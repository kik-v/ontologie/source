import xml.etree.ElementTree as ET
import rdflib
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, DC
onzfin = Namespace('http://purl.org/ozo/onz-fin#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
dc = Namespace('http://purl.org/dc/elements/1.1/')
g = Graph()
g.bind('onz-fin', onzfin)
g.bind('onz-g', onzg)
g.bind('dc', dc)

# Read XML file
xml_file_path = "NT17_RGS_20221214/nt17/rgs/20221214/dictionary/rgs-codes-lab-nl.xml" #download op https://www.referentiegrootboekschema.nl/kbase
with open(xml_file_path, "r") as xml_file:
    xml_content = xml_file.read()

# Parse the XML content
root = ET.fromstring(xml_content)

# Find all link:label elements
label_elements = root.findall(".//{http://www.xbrl.org/2003/linkbase}label")

# create triples for each label element
for label_elem in label_elements:
    label = label_elem.text
    code = label_elem.get('id')[6:-9]

    # To get the parent code, strip the last part that starts with an uppercase character
    parts = []
    current_part = ''
    level = 0
    for char in code:
        if char.isupper():
            parts.append(current_part)
            current_part = char
            level += 1
        else:
            current_part += char
    # Add the last part
    if current_part:
        parts.append(current_part)
    # Join the parts except the last one
    parent_code = ''.join(parts[:-1])
    html_label = label.replace(' ', '_')
    html_link = 'https://www.boekhoudplaza.nl/rgs_rekeningen/' + code + '&rgsv=' + code + '/' + html_label + '.htm'
    # print(f'label: {label}, code: {code}, niveau: {level}, part of: {parent_code}')
    # print(f'dc:source = https://www.boekhoudplaza.nl/rgs_rekeningen/{code}&rgsv={code}/{html_label}.htm')

    g.add((URIRef('http://purl.org/ozo/onz-fin#' + code), RDFS.label, Literal(label)))
    g.add((URIRef('http://purl.org/ozo/onz-fin#' + code), onzg.partOf, URIRef('http://purl.org/ozo/onz-fin#' + parent_code)))
    g.add((URIRef('http://purl.org/ozo/onz-fin#' + code), DC.source, URIRef(html_link)))
    if level == 2 or level == 3:
        g.add((URIRef('http://purl.org/ozo/onz-fin#' + code), RDF.type, onzfin.Grootboekrubriek))
    if level == 4:
        g.add((URIRef('http://purl.org/ozo/onz-fin#' + code), RDF.type, onzfin.Grootboekrekening))

g.serialize(destination='Ontologie_data/RGS.ttl', format='turtle')