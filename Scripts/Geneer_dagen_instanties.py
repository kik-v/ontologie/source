from datetime import date, timedelta
from rdflib.namespace import RDF, OWL, SKOS, RDFS, XSD, NamespaceManager
from rdflib import URIRef, BNode, Literal, Graph, Namespace

DATA = Namespace("http://purl.org/ozo/onz-data/")
TIME = Namespace('http://www.w3.org/2006/time#')
g = Graph()
g.namespace_manager = NamespaceManager(Graph())
g.namespace_manager.bind('data', DATA)
g.namespace_manager.bind('time', TIME)

# Maak instanties aan voor ieder dag in een gegeven periode
# Voeg een numerieke positie toe (aantal dagen na 1-1-1900)

fname = "data.ttl"
startDatum = date(2020, 1, 1)
eindDatum = date(2024, 12, 31)

for i in range((eindDatum - startDatum).days +1):
    datum = startDatum + timedelta(days = i)
    volgnummer = (datum - date(1900, 1, 1)).days + 1
    datumURI = URIRef('http://purl.org/ozo/onz-g/dag' + str(datum))
    TemporalPosition = BNode()
    g.add((datumURI, RDF.type, TIME.Instant))
    g.add((datumURI, TIME.inXSDDate, Literal(str(datum), datatype=XSD.date)))
    g.add((datumURI, TIME.inTemporalPosition, TemporalPosition))
    g.add((TemporalPosition, RDF.type, TIME.TimePosition))
    g.add((TemporalPosition, TIME.numericPosition, Literal(volgnummer)))

#print(g.serialize())
g.serialize(destination='Ontologie data/dagen.ttl', format='turtle')