# Genereren testdata
In deze map staan python scripts om testdata te genereren. De testdata heeft twee vormen
- RDF triples (in de vorm van een turtle bestand)
- Instanties van Python objecten

Deze twee bestanden woren gebruikt in twee separate stromen om indicatoren te berekenen. Doel is om beide stromen separaat te volgen en dan de resultaten te vergelijken. Wanneer de resultaten uit beide stromen identiek zijn (tot een 'redelijk' aantal decimalen, want door afronden kunnen kleine verschillen ontstaan) is de test van die indicator geslaagd.
## Stroom 1 - op basis van de gegenereerde RDF
Laad de gegenereerde RDF data in een triple store
Laad de ontologie en bijbehorende individuals in de triple store
Voer de SPARQL queries uit die indicatoren berekenen
## Stroom 2 - op basis van python
Gebruik de gegenereerde instanties als input voor de indicatorberekening (bijv. odb_indicatoren.py in het uitwisselprofiel voor de ODB)
Vergelijk de resultaten van beide stromen met elkaar.

## generate_test_data.py
Primaire script dat testdata genereert op basis van de variabelen in variables.py

Output bestaat uit twee bestanden:
- generated_testdata.pkl
- generated_testdata.ttl

### generated_testdata.pkl
Binair bestand met daarin alle testdata in de vorm van python objecten. Deze wordt gebruikt om
- RDF te genereren
- Uitkomsten van indicatoren te berekenen via python code

### generated_testdata.ttl
Een turtle representatie van de gegenereerde testdata.

## functions.py
Bevat de functies die gebruikt worden door generate_test_data.py

## variables.py
Bevat alle variabelen waarop de testdata is gebaseerd. Naast de verschillende owl klassen waarvan data wordt gegenereerd bevat deze ook de periodes waarover testdata wordt gegenereerd en de hoeveelheid testdata die wordt gegenereerd.

## rdf_objecten.py
Bevat de definities van alle klassen waarvan instanties worden aangemaakt.