import random
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, DC, OWL
from datetime import datetime, timedelta, date
import json

# prefixes
onzorg = Namespace('http://purl.org/ozo/onz-org#')
onzzorg = Namespace('http://purl.org/ozo/onz-zorg#')
onzpers = Namespace('http://purl.org/ozo/onz-pers#')
onzg = Namespace('http://purl.org/ozo/onz-g#')
onzfin = Namespace('http://purl.org/ozo/onz-fin#')
dummy = Namespace('http://data.dummyzorg.nl/')

zorg_owl_file = '../onz-zorg.owl'
prismant_ttl_file = '../Ontologie_data/PrismantGrootboekIndividuals.ttl'
rgs_ttl_file = '../Ontologie_data/RGS.ttl'
client_data_file = 'generated_test_clients.pkl'
fin_data_file = 'generated_fin_data.pkl'
fin_data_turtle = 'generated_fin_data.ttl'

g = Graph()
zorg_ontologie = Graph()
g.bind('onz-org', onzorg)
g.bind('onz-zorg', onzzorg)
g.bind('onz-g', onzg)
g.bind('onz-pers', onzpers)
g.bind('onz-fin', onzfin)
g.bind('dummy', dummy)

# files
zorg_owl_file = '../onz-zorg.owl'
test_data_file = 'generated_testdata.pkl'
test_data_turtle = 'generated_testdata.ttl'
client_data_file = 'generated_test_clients.pkl'
fin_data_file = 'generated_fin_data.pkl'
fin_data_turtle = 'generated_fin_data.ttl'

WerkOvereenkomst_types = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.VrijwilligersOvereenkomst, onzpers.StageOvereenkomst, onzpers.InhuurOvereenkomst]
ArbeidsOvereenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
PnilOvereenkomsten = [onzpers.InhuurOvereenkomst, onzpers.UitzendOvereenkomst]
Functies = [dummy.Verpleegkundige_A_Functie, dummy.Verpleegkundige_B_Functie, dummy.Verpleegkundige_C_Functie, dummy.Verpleegkundige_D_Functie, dummy.Verpleegkundige_E_Functie, dummy.Verpleegkundige_F_Functie, dummy.Teamhoofd_Functie]
ZorgFuncties = [dummy.Verpleegkundige_A_Functie, dummy.Verpleegkundige_B_Functie, dummy.Verpleegkundige_C_Functie, dummy.Verpleegkundige_D_Functie, dummy.Verpleegkundige_E_Functie, dummy.Verpleegkundige_F_Functie]
Vestigingen = [dummy.Vestiging_De_Beuk, dummy.Vestiging_Grotestraat]
FunctieNiveaus = {dummy.Verpleegkundige_A_Functie: onzpers.Kwalificatieniveau_1, dummy.Verpleegkundige_B_Functie: onzpers.Kwalificatieniveau_2, dummy.Verpleegkundige_C_Functie: onzpers.Kwalificatieniveau_3, dummy.Verpleegkundige_D_Functie: onzpers.Kwalificatieniveau_4, dummy.Verpleegkundige_E_Functie: onzpers.Kwalificatieniveau_5, dummy.Verpleegkundige_F_Functie: onzpers.Kwalificatieniveau_6}

with open('waardelijsten.json', 'r') as waardelijsten_file:
    waardelijsten = json.load(waardelijsten_file)
ProductCodes = waardelijsten['declaratieCodes']['productCodes']['GRZ']  # ["123", "234", "345"]
# PrestatieCodes = ["pc_1000", "pc_1001", "pc_1002", "pc_1003", "tv_1028"]
PrestatieCodes = waardelijsten['declaratieCodes']['prestatieCodes']
PrestatieCodes_flat = [*PrestatieCodes['ELV'], *PrestatieCodes['WLZ'], *PrestatieCodes['WMO'], *PrestatieCodes['ZVW']]

num_work_agreements = 3
num_people = 100
num_clients = 5
parttime_factor = [0.2, 0.5, 0.8, 1.0]
uren_per_week = [8, 16, 24, 32, 36, 40]
ziekte_percentages = [100, 80, 50, 25]

timeframe_start = date(2022, 1, 1)
timeframe_end = date(2024, 12, 31)

query_start = date(2023, 1, 1)#.date().isoformat()
query_end = date(2023, 12, 31)#.date().isoformat()
peildatum = date(2023, 12, 31)#.date().isoformat()
meetperiode = (query_start, query_end)

# zorg_ontologie.load(zorg_owl_file)  # AttributeError: 'Graph' object has no attribute 'load'
zorg_ontologie.parse(zorg_owl_file)
zorgprofielen = list(zorg_ontologie.subjects(RDF.type, onzzorg.ZorgProfiel))
ggzb_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, URIRef("http://purl.org/ozo/onz-zorg#GGZ-B")))
ggzw_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, URIRef("http://purl.org/ozo/onz-zorg#GGZ-W")))
lg_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, onzzorg.LG))
lvg_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, onzzorg.LVG))
vv_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, onzzorg.VV))
vg_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, onzzorg.VG))
zgaud_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, onzzorg.ZGAUD))
zgvis_zorgprofielen = list(zorg_ontologie.subjects(onzg.isAbout, onzzorg.ZGVIS))
leveringsvormen = list(zorg_ontologie.subjects(RDF.type, onzzorg.Leveringsvorm))
indicatie_besluiten = list(zorg_ontologie.subjects(RDFS.subClassOf, onzzorg.IndicatieBesluit))

# parse PrismantGrootboekIndividuals.ttl to extract possible codes
prismant_individuals = Graph()
prismant_individuals.parse(prismant_ttl_file)
prismant_rubrieken = [r[0] for r in prismant_individuals.query('''
    PREFIX onz-fin: <http://purl.org/ozo/onz-fin#> 
    
    SELECT *
    WHERE {
        ?rubriek a onz-fin:Grootboekrubriek .
    }
    ''')]

# parse RGS.ttl to extract possible codes
rgs_individuals = Graph()
rgs_individuals.parse(rgs_ttl_file)
rgs_rubrieken = [r[0] for r in rgs_individuals.query('''
    PREFIX onz-fin: <http://purl.org/ozo/onz-fin#> 
    
    SELECT *
    WHERE {
        ?rubriek a onz-fin:Grootboekrekening .
    }
    ''')]
    
rgs_hierarchy = {}
if 0:
    rgs_partOfRelationships = [r for r in rgs_individuals.query('''
        PREFIX onz-g: <http://purl.org/ozo/onz-g#>
        PREFIX onz-fin: <http://purl.org/ozo/onz-fin#>
        SELECT DISTINCT ?rubriek ?subrubriek
        WHERE {                                                 
            {?rubriek a onz-fin:Grootboekrekening .}
            UNION {?rubriek a onz-fin:Grootboekrubriek .}
            ?subrubriek onz-g:partOf* ?rubriek .
            FILTER NOT EXISTS {?subrubriek a onz-fin:Grootboekpost.}
            FILTER (?subrubriek != ?rubriek)
        }
        ''')]

    for (parentRubriek, childRubriek) in rgs_partOfRelationships: 
        parentRubriek_id = str(parentRubriek).split('onz-fin#')[1]
        childRubriek_id = str(childRubriek).split('onz-fin#')[1]
        if parentRubriek_id not in rgs_hierarchy:
            rgs_hierarchy[parentRubriek_id] = [childRubriek_id]
        else:
            rgs_hierarchy[parentRubriek_id].append(childRubriek_id)

    with open('rgs_hierarchy.json', 'w') as outfile: 
        json.dump(rgs_hierarchy, outfile, indent = 4)


with open('rgs_hierarchy.json') as infile:
    rgs_hierarchy = json.load(infile)
