from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, DC
from variables import *
from functions import *
from collections import defaultdict
from dataclasses import dataclass, field
from typing import List, Optional, Union, Tuple
from datetime import date

@dataclass
class Client():
    subject: URIRef

    def to_rdf(self):
        g=Graph()
        g.add((self.subject, RDF.type, onzg.Human))
        return g

@dataclass
class Indicatie:
    subject: URIRef
    rdf_type: URIRef
    client: Client
    start_datum: date
    eind_datum: date
    leveringsvorm: URIRef
    zorgprofiel: URIRef
    indicatie_behandeling: bool
    
    def to_rdf(self):
        g = Graph()
        g.add((self.subject, RDF.type, self.rdf_type))
        g.add((self.subject, onzg.isAbout, self.client.subject))
        g.add((self.subject, onzg.hasPart, self.leveringsvorm))
        g.add((self.subject, onzg.hasPart, self.zorgprofiel))
        g.add((self.subject, onzg.startDatum, Literal(self.start_datum, datatype=XSD.date)))
        if self.eind_datum:
            g.add((self.subject, onzg.eindDatum, Literal(self.eind_datum, datatype=XSD.date)))

        return g

@dataclass
class ZorgProces:
    subject: URIRef
    locatie: URIRef
    deelnemer: Client
    indicatie: Indicatie
    periode: Tuple

    def to_rdf(self):
        g = Graph()
        g.add((self.subject, RDF.type, onzzorg.NursingProcess))
        g.add((self.subject, onzg.definedBy, self.indicatie.subject))
        g.add((self.subject, onzg.hasParticipant, self.deelnemer.subject))
        g.add((self.subject, onzg.startDatum, Literal(self.periode[0], datatype=XSD.date)))
        if self.periode[1]:
            g.add((self.subject, onzg.eindDatum, Literal(self.periode[1], datatype=XSD.date)))
        if self.locatie:
            g.add((self.subject, onzg.hasPerdurantLocation, self.locatie))
        return g

@dataclass
class WoonEenheid:
    subject: URIRef
    locatie: URIRef
    bewoners: List
    capaciteit: int # capaciteit altijd in aantal mensen

    def add_occupant(self, bewoner):
        if bewoner not in self.bewoners:
            self.bewoners.append(bewoner)
    
    def to_rdf(self):
        g = Graph()
        g.add((self.subject, RDF.type, onzorg.WoonEenheid))
        g.add((self.subject, onzg.partOf, self.locatie))
        # bezetting
        quality = BNode()
        g.add((self.subject, onzg.hasQuality, quality))
        g.add((quality, RDF.type, onzg.OccupancyQuality))
        for bewoner in self.bewoners:
            quality_value = BNode()
            g.add((quality_value, RDF.type, onzg.OccupancyValue))
            g.add((quality, onzg.hasQualityValue, quality_value))
            g.add((quality_value, onzg.hasOccupancyObject, bewoner.subject))
        # capaciteit
        cap_quality = BNode()
        g.add((cap_quality, RDF.type, onzg.Quality))
        g.add((self.subject, onzg.hasQuality, cap_quality))
        cap_value = BNode()
        g.add((cap_value, RDF.type, onzg.CapacityValue))
        g.add((cap_quality, onzg.hasQualityValue, cap_value))
        g.add((cap_value, onzg.hasCapacityObject, onzg.Human))
        g.add((cap_value, onzg.hasDataValue, Literal(self.capaciteit, datatype=XSD.integer)))
        return g

