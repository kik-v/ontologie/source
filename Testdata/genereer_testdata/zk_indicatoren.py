from variables import *
from functions import *
from rdf_client_objecten import *
from rdf_fin_objecten import *

def indicator_zk_1_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["zorg"] = 0
    indicator["Totaal"]["niet-zorg"] = 0
    indicator["Totaal"]["totaal"] = 0

    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    dagen_periode = days_in_period(periode)
    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    zorg_functie = werkovereenkomstafspraak.get_functie() in ZorgFuncties
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    meetperiode = trim_to_period(contract_periode, periode)
                    if meetperiode:
                        if locatie not in indicator:
                            indicator[locatie] = {}
                            indicator[locatie]["zorg"] = 0
                            indicator[locatie]["niet-zorg"] = 0
                            indicator[locatie]["totaal"] = 0
                        indicator[locatie]["totaal"] += days_in_period(meetperiode)/dagen_periode
                        indicator["Totaal"]["totaal"] += days_in_period(meetperiode)/dagen_periode
                        if zorg_functie:
                            indicator[locatie]["zorg"] += days_in_period(meetperiode)/dagen_periode
                            indicator["Totaal"]["zorg"] += days_in_period(meetperiode)/dagen_periode
                        else:
                            indicator[locatie]["niet-zorg"] += days_in_period(meetperiode)/dagen_periode
                            indicator["Totaal"]["niet-zorg"] += days_in_period(meetperiode)/dagen_periode
    output_string = f"\nZK 1.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ':'
        for zorg in indicator[vestiging]:
            output_string += ' ' + zorg + ': ' + str(indicator[vestiging][zorg])
    return (output_string)

def indicator_zk_1_2(datum, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["zorg"] = set()
    indicator["Totaal"]["niet-zorg"] = set()
    indicator["Totaal"]["totaal"] = set()

    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    zorg_functie = werkovereenkomstafspraak.get_functie() in ZorgFuncties
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(datum, contract_periode):
                        if locatie not in indicator:
                            indicator[locatie] = {}
                            indicator[locatie]["zorg"] = set()
                            indicator[locatie]["niet-zorg"] = set()
                            indicator[locatie]["totaal"] = set()
                        indicator[locatie]["totaal"].add(mens)
                        indicator["Totaal"]["totaal"].add(mens)
                        if zorg_functie:
                            indicator[locatie]["zorg"].add(mens)
                            indicator["Totaal"]["zorg"].add(mens)
                        else:
                            indicator[locatie]["niet-zorg"].add(mens)
                            indicator["Totaal"]["niet-zorg"].add(mens)
    output_string = f"\nZK 1.2 indicator datum: {datum}"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ':'
        for zorg in indicator[vestiging]:
            output_string += ' ' + zorg + ': ' + str(len(indicator[vestiging][zorg]))
    return (output_string)

def indicator_zk_2_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = 0
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = 0
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        contract_periode = werkovereenkomstafspraak.get_periode()
                        meetperiode = trim_to_period(contract_periode, periode)
                        if meetperiode:
                            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                            for gewerkte_periode in gewerkte_perioden:
                                if date_in_period(gewerkte_periode.get_date(), meetperiode):
                                        werklocatie = gewerkte_periode.get_locatie().split('/')[-1]
                                        if werklocatie:
                                            indicator[werklocatie] += gewerkte_periode.get_uren()
                                        else:
                                            indicator[locatie] += gewerkte_periode.get_uren()
                                        indicator["Totaal"] += gewerkte_periode.get_uren()
    output_string = f"\nZK 2.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ': ' + str(indicator[vestiging])
    return (output_string)

def indicator_zk_2_2(jaar, kwartaal, financial_instances, test_instances):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.UitzendOvereenkomst, onzpers.InhuurOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["zorg"] = 0
    indicator["Totaal"]["niet-zorg"] = 0
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {}
        indicator[vestiging.split('/')[-1]]["zorg"] = 0
        indicator[vestiging.split('/')[-1]]["niet-zorg"] = 0
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    output_string = f"\nZK 2.2 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    verloonde_perioden = get_objects_from(instances=financial_instances, object_type=VerloondePeriode)
    for verloonde_periode in verloonde_perioden:
        uren_periode = (verloonde_periode.start_datum, verloonde_periode.eind_datum)
        if period1_inperiod2(uren_periode, periode):
            # Bepaal of de geldige werkafspraak een zorg of niet-zorg functie bevat
            overeenkomst = verloonde_periode.overeenkomst
            type_overeenkomst = overeenkomst.get_rdftype()
            if type_overeenkomst in geldige_overeenkomsten:
                zorg = is_care_on_date(werk_overeenkomst=overeenkomst, date=verloonde_periode.start_datum)
                werk_afspraak = get_afspraak_on_date(werk_overeenkomst=overeenkomst, date=verloonde_periode.start_datum)
                
                if werk_afspraak:
                    vestiging = werk_afspraak.get_werklocatie().split('/')[-1]
                else:
                    print("Uren gewerkt zonder geldige werkafspraak....")
                if zorg:
                    indicator["Totaal"]["zorg"] += verloonde_periode.aantal_uren
                    if vestiging:
                        indicator[vestiging]["zorg"] += verloonde_periode.aantal_uren
                else:
                    indicator["Totaal"]["niet-zorg"] += verloonde_periode.aantal_uren
                    if vestiging:
                        indicator[vestiging]["niet-zorg"] += verloonde_periode.aantal_uren
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ': ' + str(indicator[vestiging])
    return (output_string)

def indicator_zk_3_1(peildatum, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["bepaald"] = 0
    indicator["Totaal"]["onbepaald"] = 0
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {}
        indicator[vestiging.split('/')[-1]]["bepaald"] = 0
        indicator[vestiging.split('/')[-1]]["onbepaald"] = 0

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            type_overeenkomst = werkovereenkomst.get_rdftype()
            if type_overeenkomst in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if date_in_period(peildatum, contract_periode):
                        if type_overeenkomst == onzpers.ArbeidsOvereenkomstBepaaldeTijd:
                            indicator["Totaal"]["bepaald"] += 1
                            indicator[locatie]["bepaald"] += 1
                        else:
                            indicator["Totaal"]["onbepaald"] += 1
                            indicator[locatie]["onbepaald"] += 1

    output_string = f"\nZK 3.1 indicator peildatum {peildatum}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ':'
        for type_overeenkomst in indicator[vestiging]:
            output_string += ' ' + type_overeenkomst + ': ' + str(indicator[vestiging][type_overeenkomst])
    return (output_string)

def indicator_zk_4_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    uniek = {}
    for locatie_url in Vestigingen:
        locatie = locatie_url.split('/')[-1]
        indicator[locatie] = {"zorg": 0, "niet-zorg": 0}
        uniek[locatie] = {"zorg": set(), "niet-zorg": set()}
    indicator["Totaal"] = {"zorg": 0, "niet-zorg": 0}
    uniek["Totaal"] = {"zorg": set(), "niet-zorg": set()}
    maand = 3 * int(kwartaal[-1:]) - 2
    peildatum = date(jaar, maand, 1)
    for mens in mensen:
        # teller = {"zorg": 0, "niet-zorg": 0}
        noemer = {"totaal":0, "zorg": 0, "niet-zorg": 0}
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=peildatum)
                if werkovereenkomstafspraak:
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                    omvang_waarde = contract_omvang.get_waarde()
                    omvang_eenheid = contract_omvang.get_eenheid()
                    if omvang_eenheid == onzpers.fte_36:
                        omvang_waarde = omvang_waarde * 36 # omvang in uren/week
                    
                    functie = werkovereenkomstafspraak.get_functie()
                    wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                    indicator[locatie][wel_niet_zorg] += omvang_waarde
                    indicator["Totaal"][wel_niet_zorg] += omvang_waarde
                    uniek[locatie][wel_niet_zorg].add(mens)
                    uniek["Totaal"][wel_niet_zorg].add(mens)

    # Deel contractomvang door unieke personen om gemiddelde contractomvang per persoon te krijgen
    for vestiging in indicator:
        for zorg_niet_zorg in indicator[vestiging]:
            if len(uniek[vestiging][zorg_niet_zorg]) == 0:
                indicator[vestiging][zorg_niet_zorg] = "Ongedefinieerd"
            else:
                indicator[vestiging][zorg_niet_zorg] = indicator[vestiging][zorg_niet_zorg]/len(uniek[vestiging][zorg_niet_zorg])

    output_string = f"\nZK 4.1 indicator jaar: {jaar}, kwartaal {kwartaal}:\n"
    for vestiging in indicator:
        output_string += '    ' + str(vestiging) + ': zorg ' + str(indicator[vestiging]["zorg"]) + ', niet-zorg ' + str(indicator[vestiging]["niet-zorg"]) + '\n'
    return (output_string)

def indicator_zk_5_1(jaar, kwartaal, mensen):
    indicator = {}
    unieke_mensen = {}
    unieke_mensen["Totaal"] = []
    for locatie in Vestigingen:
        indicator[locatie.split('/')[-1]] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        unieke_mensen[locatie.split('/')[-1]] = []
    indicator["Totaal"] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if period1_inperiod2(contract_periode, periode):
                        geboorte_datum = mens.get_geboortedatum()
                        # Gebruik eind van de periode om de leeftijd te berekenen
                        leeftijd = periode[1].year - geboorte_datum.year                                               
                        if (periode[1].month, periode[1].day) < (geboorte_datum.month, geboorte_datum.day):
                            leeftijd -= 1
                        categorie = int(((leeftijd-((leeftijd - 1) % 5))/5)-3)
                        if mens.get_subject() not in unieke_mensen["Totaal"]:
                            unieke_mensen["Totaal"].append(mens.get_subject())
                            indicator["Totaal"][categorie] +=1
                        if mens.get_subject() not in unieke_mensen[locatie]:
                            unieke_mensen[locatie].append(mens.get_subject())
                            indicator[locatie][categorie] +=1
    output_string = f"\nZK 5.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + vestiging + ': '
        for item in indicator[vestiging]:
            output_string += str(item) + ', '
    return (output_string)

def indicator_zk_6_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for gewerkte_periode in gewerkte_perioden:
                    dag = gewerkte_periode.get_date()
                    if date_in_period(date=dag, period=periode):
                        werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=dag)
                        functie = werkovereenkomstafspraak.get_functie()
                        if functie in ZorgFuncties:
                            functie_niveau = FunctieNiveaus[functie]
                            uren = gewerkte_periode.get_uren()
                            locatie = gewerkte_periode.get_locatie()
                            if not locatie:
                                locatie = werkovereenkomstafspraak.get_locatie()
                            if locatie:
                                if locatie not in indicator:
                                    indicator[locatie] = {}
                                if functie_niveau not in indicator[locatie]:
                                    indicator[locatie][functie_niveau] = 0
                                if functie_niveau not in indicator["Totaal"]:
                                    indicator["Totaal"][functie_niveau] = 0
                                indicator[locatie][functie_niveau] += uren
                                indicator["Totaal"][functie_niveau] += uren
    output_string = f"\nZK 6.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in sorted(indicator.keys()):
        output_string += '\n    ' + str(vestiging) + ':'
        for kwalificatieniveau in sorted(indicator[vestiging].keys()):
            output_string += '\n         ' + str(kwalificatieniveau) + ': ' + str(indicator[vestiging][kwalificatieniveau])
    output_string += '\n'
    return (output_string)

def indicator_zk_7_1(jaar, kwartaal, mensen, clienten):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst, onzpers.InhuurOvereenkomst, onzpers.UitzendOvereenkomst]
    indicator = {}
    unique_clients = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = [0, 0, 0]
        unique_clients[vestiging.split('/')[-1]] = set()
    indicator["Totaal"] = [0, 0, 0]
    unique_clients["Totaal"] = set()
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        contract_locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        contract_periode = werkovereenkomstafspraak.get_periode()
                        meetperiode = trim_to_period(contract_periode, periode)
                        if meetperiode:
                            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                            for gewerkte_periode in gewerkte_perioden:
                                if date_in_period(gewerkte_periode.get_date(), meetperiode):
                                        werklocatie = gewerkte_periode.get_locatie().split('/')[-1]
                                        locatie = werklocatie if werklocatie else contract_locatie
                                        indicator[locatie][0] += gewerkte_periode.get_uren()
                                        indicator["Totaal"][0] += gewerkte_periode.get_uren()
    for zorg_proces in [instance for instance in clienten if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        indicatie = zorg_proces.indicatie
        zorg_profiel = indicatie.zorgprofiel
        if zorg_profiel in vv_zorgprofielen and period1_inperiod2(zorg_periode, periode):
            vestiging = zorg_proces.locatie.split('/')[-1]
            unique_clients[vestiging].add(zorg_proces.deelnemer.subject)
            unique_clients["Totaal"].add(zorg_proces.deelnemer.subject)
    for key in indicator:
        indicator[key][1] = len(unique_clients[key])
        indicator[key][2] = indicator[key][0] / indicator[key][1] if indicator[key][1] > 0 else "Ongedefineerd"

    output_string = f"\nZK 7.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator:
        output_string += '\n    ' + str(vestiging) + ': ' + str(indicator[vestiging])
    output_string += '\n'
    return (output_string)

def indicator_zk_8_1(jaar, kwartaal, mensen):
    indicator = {}
    indicator["Totaal"] = {"uren_pil":0, "uren_pnil": 0}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"uren_pil":0, "uren_pnil": 0}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
            for gewerkte_periode in gewerkte_perioden:
                dag = gewerkte_periode.get_date()
                if date_in_period(date=dag, period=periode):
                    werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=dag)
                    uren = gewerkte_periode.get_uren()
                    locatie = gewerkte_periode.get_locatie()
                    if not locatie:
                        locatie = werkovereenkomstafspraak.get_locatie()
                    if locatie:
                        if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:                           
                            indicator[locatie.split('/')[-1]]["uren_pil"] += uren
                            indicator["Totaal"]["uren_pil"] += uren
                        elif werkovereenkomst.get_rdftype() in PnilOvereenkomsten:
                            indicator[locatie.split('/')[-1]]["uren_pnil"] += uren
                            indicator["Totaal"]["uren_pnil"] += uren
    output_string = f"\nZK 8.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in sorted(indicator.keys()):
        output_string += '\n    ' + str(vestiging) + ':'
        for p_p in indicator[vestiging]:
            output_string += f"{p_p}: {indicator[vestiging][p_p]}  "
        output_string += 'percentage: ' + str(100*indicator[vestiging]["uren_pnil"]/(indicator[vestiging]["uren_pnil"] + indicator[vestiging]["uren_pil"]))
    output_string += '\n'
    return (output_string)

def indicator_zk_8_2(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 8.2 - Percentage kosten personeel niet in loondienst (PNIL): {start_periode}, eind_periode: {eind_periode}'
    print(output_string)
    totaal_pil, totaal_pnil = 0, 0
    rubrieken_pil = {
        "WPerLes",
        "WPerSol",
        "411000",
        "412000",
        "413000",
        "414000",
        "415000",
        "422100",
        "422300",
        "422400",
        "422410",
        "422500",
        "422600",
        "422900",
    }
    rubrieken_pil = expand_rubrieken(rubrieken_pil)  # expand list with children rubrieken for RGS
    
    rubrieken_pnil = [
        "WBedOvpUik",
        "WBedOvpUit",
        "WBedOvpMaf",
        "WBedOvpZzp",
        "WBedOvpPay",
        "WBedOvpOip",
        "418000"
    ]
    rubrieken_pnil = expand_rubrieken(rubrieken_pnil)  # expand list with children rubrieken for RGS
    
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id in rubrieken_pil:
                totaal_pil += post.price
            if rubriek_id in rubrieken_pnil:
                totaal_pnil += post.price
    totaal = totaal_pnil + totaal_pil

    output_string += f'\n    Totaal organisatie: Kosten PIL: {round(totaal_pil, 2)}, Kosten PNIL: {round(totaal_pnil, 2)}, Totaal:{round(totaal, 2)}'
    return output_string

# file.write(indicator_zk_8_2(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')


def indicator_zk_9_1(jaar, kwartaal, mensen):
    indicator = {}
    indicator["Totaal"] = []
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = []

    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    periode = (startdatum, startdatum + relativedelta(months=3) - timedelta(days=1))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() == onzpers.VrijwilligersOvereenkomst:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if period1_inperiod2(period1=periode, period2=werkovereenkomstafspraak.get_periode()):
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        if mens.get_subject() not in indicator[locatie]:
                            indicator[locatie].append(mens.get_subject())
                        if mens.get_subject() not in indicator["Totaal"]:    
                            indicator["Totaal"].append(mens.get_subject())
    output_string = f"\nZK 9.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in sorted(indicator.keys()):
        output_string += '\n    ' + str(vestiging) + ':' + str(len(indicator[vestiging]))
    output_string += '\n'
    return (output_string)

def indicator_zk_11_1(jaar, kwartaal, mensen):
    indicator = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    indicator["Totaal"] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    einddatum = startdatum + relativedelta(months=3) - timedelta(days=1)
    periode = (startdatum, einddatum)
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36 # omvang in fte
                        functie = werkovereenkomstafspraak.get_functie()
                        wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator['Totaal'][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        indicator[locatie][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                            verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                            #Ziekte en/of Zwangerschap
                            if verzuim_periode and verzuim.get_type() == onzpers.ZiektePeriode:
                                totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                #kort/langdurend verzuim
                                if totaal_verzuim_dagen <= 28:
                                    hersteld = 0
                                    verzuim_corr = days_in_period(verzuim_periode)
                                    for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                        verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                        if verzuimtijd_periode:
                                            verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                            vp = verzuimtijd.get_ziekte_percentage() / 100
                                            hersteld += verzuimtijd_dagen * (1-vp)
                                    indicator['Totaal'][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)
                                    indicator[locatie][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nZK 11.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n' + vestiging 
        zorg_indicator = str(100 * indicator[vestiging]["zorg"]["teller"]/indicator[vestiging]["zorg"]["noemer"]) if indicator[vestiging]["zorg"]["noemer"] != 0 else "Ongedefinieerd"
        niet_zorg_indicator = str(100 * indicator[vestiging]["niet-zorg"]["teller"]/indicator[vestiging]["niet-zorg"]["noemer"]) if indicator[vestiging]["niet-zorg"]["noemer"] != 0 else "Ongedefinieerd"
        output_string += ', zorg: ' + zorg_indicator
        output_string += ', niet-zorg: ' + niet_zorg_indicator
    return output_string

def indicator_zk_11_2(jaar, kwartaal, mensen):
    indicator = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    indicator["Totaal"] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    einddatum = startdatum + relativedelta(months=3) - timedelta(days=1)
    periode = (startdatum, einddatum)
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36 # omvang in fte
                        functie = werkovereenkomstafspraak.get_functie()
                        wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator['Totaal'][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        indicator[locatie][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                            verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                            #Ziekte en/of Zwangerschap
                            if verzuim_periode and (verzuim.get_type() == onzpers.ZiektePeriode or verzuim.get_type() == onzpers.ZwangerschapsVerlof):
                                totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                #kort/langdurend verzuim
                                if totaal_verzuim_dagen <= 28:
                                    hersteld = 0
                                    verzuim_corr = days_in_period(verzuim_periode)
                                    for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                        verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                        if verzuimtijd_periode:
                                            verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                            vp = verzuimtijd.get_ziekte_percentage() / 100
                                            hersteld += verzuimtijd_dagen * (1-vp)
                                    indicator['Totaal'][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)
                                    indicator[locatie][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nZK 11.2 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n' + vestiging 
        zorg_indicator = str(100 * indicator[vestiging]["zorg"]["teller"]/indicator[vestiging]["zorg"]["noemer"]) if indicator[vestiging]["zorg"]["noemer"] != 0 else "Ongedefinieerd"
        niet_zorg_indicator = str(100 * indicator[vestiging]["niet-zorg"]["teller"]/indicator[vestiging]["niet-zorg"]["noemer"]) if indicator[vestiging]["niet-zorg"]["noemer"] != 0 else "Ongedefinieerd"
        output_string += ', zorg: ' + zorg_indicator
        output_string += ', niet-zorg: ' + niet_zorg_indicator
    return output_string

def indicator_zk_11_3(jaar, kwartaal, mensen):
    indicator = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    indicator["Totaal"] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    einddatum = startdatum + relativedelta(months=3) - timedelta(days=1)
    periode = (startdatum, einddatum)
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36 # omvang in fte
                        functie = werkovereenkomstafspraak.get_functie()
                        wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator['Totaal'][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        indicator[locatie][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                            verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                            #Ziekte en/of Zwangerschap
                            if verzuim_periode and verzuim.get_type() == onzpers.ZiektePeriode:
                                totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                #kort/langdurend verzuim
                                if totaal_verzuim_dagen > 28:
                                    hersteld = 0
                                    verzuim_corr = days_in_period(verzuim_periode)
                                    for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                        verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                        if verzuimtijd_periode:
                                            verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                            vp = verzuimtijd.get_ziekte_percentage() / 100
                                            hersteld += verzuimtijd_dagen * (1-vp)
                                    indicator['Totaal'][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)
                                    indicator[locatie][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nZK 11.3 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n' + vestiging 
        zorg_indicator = str(100 * indicator[vestiging]["zorg"]["teller"]/indicator[vestiging]["zorg"]["noemer"]) if indicator[vestiging]["zorg"]["noemer"] != 0 else "Ongedefinieerd"
        niet_zorg_indicator = str(100 * indicator[vestiging]["niet-zorg"]["teller"]/indicator[vestiging]["niet-zorg"]["noemer"]) if indicator[vestiging]["niet-zorg"]["noemer"] != 0 else "Ongedefinieerd"
        output_string += ', zorg: ' + zorg_indicator
        output_string += ', niet-zorg: ' + niet_zorg_indicator
    return output_string

def indicator_zk_11_4(jaar, kwartaal, mensen):
    indicator = {}
    for vestiging in Vestigingen:
        indicator[vestiging.split('/')[-1]] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    indicator["Totaal"] = {"zorg": {"teller": 0, "noemer": 0}, "niet-zorg": {"teller": 0, "noemer": 0}}
    maand = 3 * int(kwartaal[-1:]) - 2
    startdatum = date(jaar, maand, 1)
    einddatum = startdatum + relativedelta(months=3) - timedelta(days=1)
    periode = (startdatum, einddatum)
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36 # omvang in fte
                        functie = werkovereenkomstafspraak.get_functie()
                        wel_niet_zorg = "zorg" if functie in ZorgFuncties else "niet-zorg"
                        locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                        dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                        indicator['Totaal'][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        indicator[locatie][wel_niet_zorg]["noemer"] += dagen_afspraak * ptf
                        for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                            verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                            #Ziekte en/of Zwangerschap
                            if verzuim_periode and (verzuim.get_type() == onzpers.ZiektePeriode or verzuim.get_type() == onzpers.ZwangerschapsVerlof):
                                totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                #kort/langdurend verzuim
                                if totaal_verzuim_dagen > 28:
                                    hersteld = 0
                                    verzuim_corr = days_in_period(verzuim_periode)
                                    for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                        verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                        if verzuimtijd_periode:
                                            verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                            vp = verzuimtijd.get_ziekte_percentage() / 100
                                            hersteld += verzuimtijd_dagen * (1-vp)
                                    indicator['Totaal'][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)
                                    indicator[locatie][wel_niet_zorg]["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nZK 11.4 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n' + vestiging 
        zorg_indicator = str(100 * indicator[vestiging]["zorg"]["teller"]/indicator[vestiging]["zorg"]["noemer"]) if indicator[vestiging]["zorg"]["noemer"] != 0 else "Ongedefinieerd"
        niet_zorg_indicator = str(100 * indicator[vestiging]["niet-zorg"]["teller"]/indicator[vestiging]["niet-zorg"]["noemer"]) if indicator[vestiging]["niet-zorg"]["noemer"] != 0 else "Ongedefinieerd"
        output_string += ', zorg: ' + zorg_indicator
        output_string += ', niet-zorg: ' + niet_zorg_indicator
    return output_string

def indicator_zk_12_1(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["overeenkomsten"] = set()
    indicator["Totaal"]["ziektes"] = set()

    maand = 3 * int(kwartaal[-1:]) - 2
    start_datum = date(jaar, maand, 1)
    eind_datum = start_datum + relativedelta(months=3) - timedelta(days=1)

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                continue
            periode = werkovereenkomst.get_periode()
            if periode[0] > eind_datum:
                continue
            if periode[1] != False and periode[1] < start_datum:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                afspraak_periode = werkovereenkomstafspraak.get_periode()
                if afspraak_periode[0] > eind_datum:
                    continue
                if afspraak_periode[1] != False and afspraak_periode[1] < start_datum:
                    continue
                locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                if locatie not in indicator.keys():
                    indicator[locatie] = {}
                    indicator[locatie]["overeenkomsten"] = set()
                    indicator[locatie]["ziektes"] = set()
                indicator[locatie]["overeenkomsten"].add(werkovereenkomst)
                indicator["Totaal"]["overeenkomsten"].add(werkovereenkomst)

                for verzuimperiode in werkovereenkomst.get_verzuim_periode_lijst():
                    if verzuimperiode.get_type() != onzpers.ZiektePeriode:
                        continue
                    verzuim_periode = verzuimperiode.get_periode()
                    if verzuim_periode[0] > eind_datum:
                        continue
                    if verzuim_periode[0] > afspraak_periode[1]:
                        continue
                    if verzuim_periode[0] < afspraak_periode[0]:
                        continue
                    if verzuim_periode[0] < start_datum:
                        continue
                    if verzuim_periode[1] != None and verzuim_periode[1] < start_datum:
                        continue
                    if verzuim_periode[1] != None and verzuim_periode[1] < afspraak_periode[0]:
                        continue
                    indicator[locatie]["ziektes"].add(verzuimperiode)
                    indicator["Totaal"]["ziektes"].add(verzuimperiode)
    
    output_string = f"\nZK 12.1 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n    ' + str(vestiging) + ':'
        for type in indicator[vestiging]:
            output_string += ' ' + type + ': ' + str(len(indicator[vestiging][type]))
        output_string += ' frequentie: ' + str(len(indicator[vestiging]["ziektes"]) / len(indicator[vestiging]["overeenkomsten"]))
    return (output_string)

def indicator_zk_12_2(jaar, kwartaal, mensen):
    geldige_overeenkomsten = [onzpers.ArbeidsOvereenkomstBepaaldeTijd, onzpers.ArbeidsOvereenkomstOnbepaaldeTijd, onzpers.OproepOvereenkomst]
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["overeenkomsten"] = set()
    indicator["Totaal"]["ziektes"] = set()

    maand = 3 * int(kwartaal[-1:]) - 2
    start_datum = date(jaar, maand, 1)
    eind_datum = start_datum + relativedelta(months=3) - timedelta(days=1)

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in geldige_overeenkomsten:
                continue
            periode = werkovereenkomst.get_periode()
            if periode[0] > eind_datum:
                continue
            if periode[1] != False and periode[1] < start_datum:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                afspraak_periode = werkovereenkomstafspraak.get_periode()
                if afspraak_periode[0] > eind_datum:
                    continue
                if afspraak_periode[1] != False and afspraak_periode[1] < start_datum:
                    continue
                locatie = werkovereenkomstafspraak.get_werklocatie().split('/')[-1]
                if locatie not in indicator.keys():
                    indicator[locatie] = {}
                    indicator[locatie]["overeenkomsten"] = set()
                    indicator[locatie]["ziektes"] = set()
                indicator[locatie]["overeenkomsten"].add(werkovereenkomst)
                indicator["Totaal"]["overeenkomsten"].add(werkovereenkomst)

                for verzuimperiode in werkovereenkomst.get_verzuim_periode_lijst():
                    if verzuimperiode.get_type() != onzpers.ZiektePeriode and verzuimperiode.get_type() != onzpers.ZwangerschapsVerlof:
                        continue
                    verzuim_periode = verzuimperiode.get_periode()
                    if verzuim_periode[0] > eind_datum:
                        continue
                    if verzuim_periode[0] > afspraak_periode[1]:
                        continue
                    if verzuim_periode[0] < afspraak_periode[0]:
                        continue
                    if verzuim_periode[0] < start_datum:
                        continue
                    if verzuim_periode[1] != None and verzuim_periode[1] < start_datum:
                        continue
                    if verzuim_periode[1] != None and verzuim_periode[1] < afspraak_periode[0]:
                        continue
                    
                    indicator[locatie]["ziektes"].add(verzuimperiode)
                    indicator["Totaal"]["ziektes"].add(verzuimperiode)
    
    output_string = f"\nZK 12.2 indicator jaar: {jaar}, kwartaal {kwartaal}:"
    for vestiging in indicator.keys():
        output_string += '\n    ' + str(vestiging) + ':'
        for type in indicator[vestiging]:
            output_string += ' ' + type + ': ' + str(len(indicator[vestiging][type]))
        output_string += ' frequentie: ' + str(len(indicator[vestiging]["ziektes"]) / len(indicator[vestiging]["overeenkomsten"]))
    return (output_string)

def indicator_zk_13_1(peildatum, mensen):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["mensen_peildatum"] = set()
    indicator["Totaal"]["mensen_eerder"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                if not werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                    continue
                locatie = str(werkovereenkomstafspraak.get_werklocatie()).split("/")[-1]
                if locatie not in indicator:
                        indicator[locatie] = {}
                        indicator[locatie]["mensen_peildatum"] = set()
                        indicator[locatie]["mensen_eerder"] = set()
                if date_in_period(peildatum, werkovereenkomstafspraak.get_periode()):
                    indicator[locatie]["mensen_peildatum"].add(mens.get_subject())
                    indicator["Totaal"]["mensen_peildatum"].add(mens.get_subject())
                if date_in_period(peildatum - relativedelta(years=1), werkovereenkomstafspraak.get_periode()):
                    indicator[locatie]["mensen_eerder"].add(mens.get_subject())
                    indicator["Totaal"]["mensen_eerder"].add(mens.get_subject())
    
    output_string = f"\nZK 13.1 indicator peildatum: {peildatum}:"
    for key in indicator.keys():
        noemer = len(indicator[key]["mensen_peildatum"])
        teller = len(indicator[key]["mensen_peildatum"] - indicator[key]["mensen_eerder"])
        output_string += f"\n   {key}: noemer: {noemer}, teller: {teller}, indicator: {(teller / noemer * 100) if noemer != 0 else 0}"
    return (output_string)

def indicator_zk_13_2(peildatum, mensen):
    indicator = {}
    indicator["Totaal"] = {}
    indicator["Totaal"]["mensen_peildatum"] = set()
    indicator["Totaal"]["mensen_eerder"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                if not werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                    continue
                locatie = str(werkovereenkomstafspraak.get_werklocatie()).split("/")[-1]
                if locatie not in indicator:
                        indicator[locatie] = {}
                        indicator[locatie]["mensen_peildatum"] = set()
                        indicator[locatie]["mensen_eerder"] = set()
                if date_in_period(peildatum, werkovereenkomstafspraak.get_periode()):
                    indicator[locatie]["mensen_peildatum"].add(mens.get_subject())
                    indicator["Totaal"]["mensen_peildatum"].add(mens.get_subject())
                if date_in_period(peildatum - relativedelta(years=1), werkovereenkomstafspraak.get_periode()):
                    indicator[locatie]["mensen_eerder"].add(mens.get_subject())
                    indicator["Totaal"]["mensen_eerder"].add(mens.get_subject())
    
    output_string = f"\nZK 13.2 indicator peildatum: {peildatum}:"
    for key in indicator.keys():
        noemer = len(indicator[key]["mensen_eerder"])
        teller = len(indicator[key]["mensen_eerder"] - indicator[key]["mensen_peildatum"])
        output_string += f"\n   {key}: noemer: {noemer}, teller: {teller}, indicator: {(teller / noemer * 100) if noemer != 0 else 0}"
    return (output_string)

def indicator_zk_13_3(peildatum, mensen):
    indicator = {}
    indicator = {}
    indicator["mensen_peildatum"] = set()
    indicator["mensen_eerder"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                functie = werkovereenkomstafspraak.get_functie()
                if functie not in ZorgFuncties:
                    continue

                niveau = int(str(FunctieNiveaus[functie]).split("_")[1])
                if niveau > 6 or niveau < 1:
                    continue
                
                if date_in_period(peildatum, werkovereenkomstafspraak.get_periode()):
                    indicator["mensen_peildatum"].add((mens.get_subject(), niveau))
                if date_in_period(peildatum - relativedelta(months=3), werkovereenkomstafspraak.get_periode()):
                    indicator["mensen_eerder"].add((mens.get_subject(), niveau))
    
    output_string = f"\nZK 13.3 indicator peildatum: {peildatum}:"
    noemer = 0
    teller = 0
    for (mens_peildatum, niveau_peildatum) in indicator["mensen_peildatum"]:
        for (mens_eerder, niveau_eerder) in indicator["mensen_eerder"]:
            if mens_peildatum == mens_eerder:
                noemer += 1
                if niveau_eerder < niveau_peildatum:
                    teller += 1
    output_string += f'\n    teller: {teller}, noemer: {noemer}, indicator: {100*teller/noemer if noemer != 0 else "Ongedefinieerd"}'
    return (output_string)

def indicator_zk_13_4(peildatum, mensen):
    indicator = {}
    indicator = {}
    indicator["mensen_peildatum"] = set()
    indicator["mensen_eerder"] = set()

    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if not werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                continue
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                functie = werkovereenkomstafspraak.get_functie()
                if functie not in ZorgFuncties:
                    continue

                niveau = int(str(FunctieNiveaus[functie]).split("_")[1])
                if niveau > 6 or niveau < 1:
                    continue
                
                if date_in_period(peildatum, werkovereenkomstafspraak.get_periode()):
                    indicator["mensen_peildatum"].add((mens.get_subject(), niveau))
                if date_in_period(peildatum - relativedelta(months=3), werkovereenkomstafspraak.get_periode()):
                    indicator["mensen_eerder"].add((mens.get_subject(), niveau))
    
    output_string = f"\nZK 13.4 indicator peildatum: {peildatum}:"
    noemer = 0
    teller = 0
    for (mens_peildatum, niveau_peildatum) in indicator["mensen_peildatum"]:
        for (mens_eerder, niveau_eerder) in indicator["mensen_eerder"]:
            if mens_peildatum == mens_eerder:
                noemer += 1
                if niveau_eerder > niveau_peildatum:
                    teller += 1
    output_string += f'\n    teller: {teller}, noemer: {noemer}, indicator: {100*teller/noemer if noemer != 0 else "Ongedefinieerd"}'
    return (output_string)

def indicator_zk_14_1(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nZK 14.1, peildatum: {peildatum}"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        indicatie = zorg_proces.indicatie
        zorg_profiel = indicatie.zorgprofiel
        if zorg_profiel in vv_zorgprofielen and date_in_period(peildatum, zorg_periode):
            vestiging = zorg_proces.locatie.split('/')[-1]
            if vestiging not in unique_clients:
                unique_clients[vestiging]={}
                unique_clients[vestiging][zorg_profiel] = 1
            else:
                if zorg_profiel not in unique_clients[vestiging]:
                    unique_clients[vestiging][zorg_profiel] = 1
                else:
                    unique_clients[vestiging][zorg_profiel] += 1
    for vestiging in unique_clients.keys():
        output_string += '\n    ' + vestiging + ': '
        for key, value in unique_clients[vestiging].items():
            output_string += str(key).split('#')[-1] + ': ' + str(value) + ', '
    return (output_string)

def indicator_zk_14_2(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nZK 14.2, peildatum: {peildatum}"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        indicatie = zorg_proces.indicatie
        zorg_profiel = indicatie.zorgprofiel
        if zorg_profiel in vv_zorgprofielen and date_in_period(peildatum, zorg_periode):
            vestiging = zorg_proces.locatie.split('/')[-1]
            leverings_vorm = indicatie.leveringsvorm
            if vestiging not in unique_clients:
                unique_clients[vestiging]={}
                unique_clients[vestiging][leverings_vorm] = 1
            else:
                if leverings_vorm not in unique_clients[vestiging]:
                    unique_clients[vestiging][leverings_vorm] = 1
                else:
                    unique_clients[vestiging][leverings_vorm] += 1
    for vestiging in unique_clients.keys():
        output_string += '\n    ' + vestiging + ': '
        for key, value in unique_clients[vestiging].items():
            if key == onzzorg.instelling:
                if indicatie.indicatie_behandeling:
                    key = 'Verblijf met behandeling'
                else:
                    key = 'Verblijf zonder behandeling'
            output_string += str(key).split('#')[-1] + ': ' + str(value) + ', '
    return (output_string)

def per_vestiging_indicator_zk_14_3(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nZK 14.3, peildatum: {peildatum}"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        indicatie = zorg_proces.indicatie
        zorg_profiel = indicatie.zorgprofiel
        if zorg_profiel in vv_zorgprofielen and date_in_period(peildatum, zorg_periode):
            zorg_profiel = zorg_profiel.split('#')[-1]
            leverings_vorm = indicatie.leveringsvorm
            if leverings_vorm == onzzorg.instelling:
                if indicatie.indicatie_behandeling:
                    leverings_vorm = 'Verblijf met behandeling'
                else:
                    leverings_vorm = 'Verblijf zonder behandeling'
            else:
                leverings_vorm = leverings_vorm.split('#')[-1]
            vestiging = zorg_proces.locatie.split('/')[-1]
            if vestiging not in unique_clients:
                unique_clients[vestiging]={}
            if zorg_profiel not in unique_clients[vestiging]:
                unique_clients[vestiging][zorg_profiel] = {}
            if leverings_vorm not in unique_clients[vestiging][zorg_profiel]:
                unique_clients[vestiging][zorg_profiel][leverings_vorm] = 1
            else:
                unique_clients[vestiging][zorg_profiel][leverings_vorm] += 1
    for vestiging, value in unique_clients.items():
        output_string += '\n   Vestiging: ' + vestiging
        for zorgprofiel, value in unique_clients[vestiging].items():
            for leveringsvorm, value in unique_clients[vestiging][zorgprofiel].items():
                output_string += '\n        ' + zorgprofiel + ' ' + leveringsvorm + ': ' + str(unique_clients[vestiging][zorgprofiel][leveringsvorm])
    return (output_string)

def indicator_zk_14_3(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nZK 14.3, peildatum: {peildatum}"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        indicatie = zorg_proces.indicatie
        zorg_profiel = indicatie.zorgprofiel
        if zorg_profiel in vv_zorgprofielen and date_in_period(peildatum, zorg_periode):
            zorg_profiel = zorg_profiel.split('#')[-1]
            leverings_vorm = indicatie.leveringsvorm
            if leverings_vorm == onzzorg.instelling:
                if indicatie.indicatie_behandeling:
                    leverings_vorm = 'Verblijf met behandeling'
                else:
                    leverings_vorm = 'Verblijf zonder behandeling'
            else:
                leverings_vorm = leverings_vorm.split('#')[-1]
            if zorg_profiel not in unique_clients:
                unique_clients[zorg_profiel] = {}
            if leverings_vorm not in unique_clients[zorg_profiel]:
                unique_clients[zorg_profiel][leverings_vorm] = 1
            else:
                unique_clients[zorg_profiel][leverings_vorm] += 1
    for zorgprofiel, value in unique_clients.items():
        output_string += '\n    ' + zorgprofiel
        totaal = 0
        for leveringsvorm, value in unique_clients[zorgprofiel].items():
            totaal = totaal + value
            output_string += ' ' + leveringsvorm + ': ' + str(unique_clients[zorgprofiel][leveringsvorm])
        output_string += ' Totaal: ' + str(totaal)
    return (output_string)

def indicator_zk_15_4(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nZK 15.4, peildatum: {peildatum}"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        client = zorg_proces.deelnemer.subject
        woon_vestiging = False
        for wooneenheid in [instance for instance in test_instances if isinstance(instance, WoonEenheid)]:
            if zorg_proces.deelnemer in wooneenheid.bewoners:
                woon_vestiging = wooneenheid.locatie.split('/')[-1]
        if date_in_period(peildatum, zorg_periode):
            indicatie = zorg_proces.indicatie
            type_indicatie = indicatie.rdf_type
            vestiging = zorg_proces.locatie.split('/')[-1]
            if vestiging == woon_vestiging:
                if type_indicatie == onzzorg.WlzIndicatie:
                    financiering = 'Wlz'
                elif type_indicatie == onzzorg.ZvwIndicatie:
                    financiering = 'Zvw'
                elif type_indicatie == onzzorg.WmoIndicatie:
                    financiering = 'Wmo'
                else:
                    financiering = 'Overig'
                if vestiging not in unique_clients:
                    unique_clients[vestiging] = {}
                if financiering not in unique_clients[vestiging]:
                    unique_clients[vestiging][financiering] = {}
                    unique_clients[vestiging][financiering]['totaal'] = 0
                    unique_clients[vestiging][financiering]['clienten'] = []
                if client not in unique_clients[vestiging][financiering]['clienten']:
                    unique_clients[vestiging][financiering]['clienten'].append(client)
                    unique_clients[vestiging][financiering]['totaal'] += 1
    totalen = {}
    for vestiging, value in unique_clients.items():
        output_string += f'\n    {vestiging}: '
        for financiering, value in unique_clients[vestiging].items():
            if financiering not in totalen:
                totalen[financiering] = 0
            aantal = unique_clients[vestiging][financiering]['totaal']
            totalen[financiering] += aantal
            output_string += f'{financiering}: {aantal}, '
    output_string += f'\n    Totaal organisatie: '
    for financiering, aantal in totalen.items():
        output_string += f'{financiering}: {aantal}, '
    return (output_string)

def indicator_zk_15_1(test_instances):
    totaal = 0
    totalen = {}
    output_string = f"\nZK 15.1, aantal wooneenheden"
    for woon_eenheid in [instance for instance in test_instances if isinstance(instance, WoonEenheid)]:
        vestiging = woon_eenheid.locatie.split('/')[-1]
        if vestiging not in totalen:
            totalen[vestiging]=0
        totalen[vestiging] += 1
    for vestiging, aantal in totalen.items():
        totaal += aantal
        output_string += f'\n    {vestiging}: {aantal}'
    output_string += f'\n    Totaal: {totaal}'
    return (output_string)

def indicator_zk_15_2(test_instances):
    totaal = 0
    totalen = {}
    output_string = f"\nZK 15.2, aantal bezette wooneenheden"
    for woon_eenheid in [instance for instance in test_instances if isinstance(instance, WoonEenheid)]:
        bezetting = len(woon_eenheid.bewoners) > 0
        if bezetting:
            vestiging = woon_eenheid.locatie.split('/')[-1]
            if vestiging not in totalen:
                totalen[vestiging]=0
            totalen[vestiging] += 1
    for vestiging, aantal in totalen.items():
        totaal += aantal
        output_string += f'\n    {vestiging}: {aantal}'
    output_string += f'\n    Totaal: {totaal}'
    return (output_string)

def indicator_zk_15_3(test_instances):
    totaal = 0
    totalen = {}
    output_string = f"\nZK 15.3, capaciteit bewoners"
    for woon_eenheid in [instance for instance in test_instances if isinstance(instance, WoonEenheid)]:
        capaciteit = woon_eenheid.capaciteit
        vestiging = woon_eenheid.locatie.split('/')[-1]
        if vestiging not in totalen:
            totalen[vestiging]=0
        totalen[vestiging] += capaciteit
    for vestiging, aantal in totalen.items():
        totaal += aantal
        output_string += f'\n    {vestiging}: {aantal}'
    output_string += f'\n    Totaal: {totaal}'
    return (output_string)

def indicator_zk_18_1(peildatum, test_instances):
    output_string = f'\nZK 18.1, Balans o.b.v. Grootboek (Prismant), peildatum: {peildatum}'
    totalen = {
        'A Vaste activa': 0,
        'A.I Immateriële vaste activa': 0,
        'A.II Materiële vaste activa': 0,
        'A.III Financiële vaste activa': 0,
        'B Vlottende activa': 0,
        'B.I Voorraden': 0,
        'B.II Vorderingen': 0,
        'B.III Effecten': 0,
        'B.IV Liquide middelen': 0,
        'C Totaal activa': 0,
        'D Eigen vermogen': 0,
        'D.I Gestort en opgevraagd kapitaal': 0,
        'D.II Agio': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'E Voorzieningen': 0,
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
        'H Totaal passiva': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '00':
                totalen['A.I Immateriële vaste activa'] += post.price
            elif rubriek_id[:2] == '01':
                totalen['A.II Materiële vaste activa'] += post.price
            elif rubriek_id[:2] == '03':
                totalen['A.III Financiële vaste activa'] += post.price
            elif rubriek_id[:1] == '3':
                totalen['B.I Voorraden'] += post.price
            elif rubriek_id[:2] == '12':
                totalen['B.II Vorderingen'] += post.price
            elif rubriek_id[:3] == '131':
                totalen['B.III Effecten'] += post.price
            elif rubriek_id[:3] == '132':
                totalen['B.IV Liquide middelen'] += post.price
            elif rubriek_id[:4] == '0511':
                totalen['D.I Gestort en opgevraagd kapitaal'] += post.price
            elif rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
            elif rubriek_id[:2] == '06':
                totalen['E Voorzieningen'] += post.price
            elif rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
    
    totalen['A Vaste activa'] = (totalen['A.I Immateriële vaste activa'] + 
        totalen['A.II Materiële vaste activa'] + 
        totalen['A.III Financiële vaste activa'] )
    totalen['B Vlottende activa'] = (totalen['B.I Voorraden'] + 
        totalen['B.II Vorderingen'] + 
        totalen['B.III Effecten'] +
        totalen['B.IV Liquide middelen'] )
    totalen['C Totaal activa'] = (totalen['A Vaste activa'] + 
        totalen['B Vlottende activa'] )
    totalen['D Eigen vermogen'] =(totalen['D.I Gestort en opgevraagd kapitaal'] + 
        totalen['D.II Agio'] + 
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
    totalen['H Totaal passiva'] =(totalen['D Eigen vermogen'] + 
        totalen['E Voorzieningen'] + 
        totalen['F Langlopende schulden (nog voor meer dan een jaar)'] +
        totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] )
        
    for jaarrekeningpost, bedrag in totalen.items():
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    return (output_string)

def indicator_zk_18_2(peildatum, test_instances):
    output_string = f'\nZK 18.2, Winst- en verliesrekening o.b.v. grootboek (Prismant), peildatum: {peildatum}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
        'R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten': 0,
        'R.II Andere rentebaten en soortgelijke opbrengsten': 0,
        'R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten': 0,
        'R.IV Rentelasten en soortgelijke kosten': 0,
        'R Resultaat voor belastingen': 0,
        'S.I Belastingen': 0,
        'S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen': 0,
        'S Resultaat na belastingen': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if post.datum <= peildatum:
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
            elif rubriek_id[:3] == '904':
                totalen['R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten'] += post.price
            elif rubriek_id[:3] == '900':
                totalen['R.II Andere rentebaten en soortgelijke opbrengsten'] += post.price
            elif rubriek_id[:3] == '903':
                totalen['R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten'] += post.price
            elif rubriek_id[:3] in ['485', '901']:
                totalen['R.IV Rentelasten en soortgelijke kosten'] += post.price
            elif rubriek_id[:3] == '902':
                totalen['S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen'] += post.price
    
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
    totalen['R Resultaat voor belastingen'] = (totalen['P Som der bedrijfsopbrengsten'] -
        totalen['Q Som der bedrijfslasten'] +
        totalen['R.I Opbrengst van vorderingen die tot de vaste activa behoren en van effecten'] +
        totalen['R.II Andere rentebaten en soortgelijke opbrengsten'] -
        totalen['R.III Waardeverandering van vorderingen die tot de vaste activa behoren en van effecten'] -
        totalen['R.IV Rentelasten en soortgelijke kosten'] )
    totalen['S Resultaat na belastingen'] =(totalen['R Resultaat voor belastingen'] - 
        totalen['S.I Belastingen'] + 
        totalen['S.II Aandeel in winst/verlies van ondernemingen waarin wordt deelgenomen'] )
        
    for jaarrekeningpost, bedrag in totalen.items():
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    return (output_string)

def indicator_zk_19_2(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 19.2 - Liquiditeit, Werkkapitaal op korte termijn, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Werkkapitaal op korte termijn = "B Vlottende activa" - "G Kortlopende schulden (ten hoogste 1 jaar)".
    totalen = {
        'B Vlottende activa': 0,
        'B.I Voorraden': 0,
        'B.II Vorderingen': 0,
        'B.III Effecten': 0,
        'B.IV Liquide middelen': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:1] == '3':
                totalen['B.I Voorraden'] += post.price
            elif rubriek_id[:2] == '12':
                totalen['B.II Vorderingen'] += post.price
            elif rubriek_id[:3] == '131':
                totalen['B.III Effecten'] += post.price
            elif rubriek_id[:3] == '132':
                totalen['B.IV Liquide middelen'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price

    totalen['B Vlottende activa'] = (totalen['B.I Voorraden'] + 
        totalen['B.II Vorderingen'] + 
        totalen['B.III Effecten'] +
        totalen['B.IV Liquide middelen'] )
    
    waarde = totalen['B Vlottende activa'] - totalen['G Kortlopende schulden (ten hoogste 1 jaar)']
    output_string += '\n    Kental: Werkkapitaal op korte termijn'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    output_string += f'\n    B Vlottende activa: {totalen["B Vlottende activa"]}'
    output_string += f'\n    G Kortlopende schulden (ten hoogste 1 jaar): {totalen["G Kortlopende schulden (ten hoogste 1 jaar)"]}'
    
    return (output_string)

def indicator_zk_19_3(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 19.3 - Liquiditeit, Werkkapitaalratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Werkkapitaal op korte termijn = "B Vlottende activa" - "G Kortlopende schulden (ten hoogste 1 jaar)".
    # Werkkapitaalratio = Werkkapitaal op korte termijn [zie Indicator 19.2] / "P Som der bedrijfsopbrengsten"
    totalen = {
        'B Vlottende activa': 0,
        'B.I Voorraden': 0,
        'B.II Vorderingen': 0,
        'B.III Effecten': 0,
        'B.IV Liquide middelen': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:1] == '3':
                totalen['B.I Voorraden'] += post.price
            elif rubriek_id[:2] == '12':
                totalen['B.II Vorderingen'] += post.price
            elif rubriek_id[:3] == '131':
                totalen['B.III Effecten'] += post.price
            elif rubriek_id[:3] == '132':
                totalen['B.IV Liquide middelen'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
    
    totalen['B Vlottende activa'] = (totalen['B.I Voorraden'] + 
        totalen['B.II Vorderingen'] + 
        totalen['B.III Effecten'] +
        totalen['B.IV Liquide middelen'] )
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    
    if totalen['P Som der bedrijfsopbrengsten'] != 0:
        waarde = (totalen['B Vlottende activa'] - totalen['G Kortlopende schulden (ten hoogste 1 jaar)']) / totalen['P Som der bedrijfsopbrengsten']
    else:
        waarde = 'ongedefineerd'
    output_string += '\n    Kental: Werkkapitaalratio'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    output_string += f'\n    B Vlottende activa: {totalen["B Vlottende activa"]}'
    output_string += f'\n    G Kortlopende schulden (ten hoogste 1 jaar): {totalen["G Kortlopende schulden (ten hoogste 1 jaar)"]}'
    output_string += f'\n    P Som der bedrijfsopbrengsten: {totalen["P Som der bedrijfsopbrengsten"]}'
    
    return (output_string)

def indicator_zk_19_4(start_periode, eind_periode, test_instances):
    output_string = f'\nZK 19.4 - Liquiditeit, Quick ratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Quick ratio = ("B Vlottende activa" - "B.I Voorraden") / "G Kortlopende schulden (ten hoogste 1 jaar)"
    totalen = {
        'B Vlottende activa': 0,
        'B.I Voorraden': 0,
        'B.II Vorderingen': 0,
        'B.III Effecten': 0,
        'B.IV Liquide middelen': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:1] == '3':
                totalen['B.I Voorraden'] += post.price
            elif rubriek_id[:2] == '12':
                totalen['B.II Vorderingen'] += post.price
            elif rubriek_id[:3] == '131':
                totalen['B.III Effecten'] += post.price
            elif rubriek_id[:3] == '132':
                totalen['B.IV Liquide middelen'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
    
    totalen['B Vlottende activa'] = ( # totalen['B.I Voorraden'] + 
        totalen['B.II Vorderingen'] + 
        totalen['B.III Effecten'] +
        totalen['B.IV Liquide middelen'] )
    
    if totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] != 0:
        waarde = (totalen['B Vlottende activa'] / totalen['G Kortlopende schulden (ten hoogste 1 jaar)'])
    else:
        waarde = 'ongedefineerd'
    output_string += '\n    Kental: Quick ratio'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    output_string += f'\n    B Vlottende activa - B.I: {totalen["B Vlottende activa"]}'
    output_string += f'\n    G Kortlopende schulden (ten hoogste 1 jaar): {totalen["G Kortlopende schulden (ten hoogste 1 jaar)"]}'
   
    return (output_string)

def indicator_zk_19_5(start_periode, eind_periode, test_instances):
    output_string = f'\n19.5 - Liquiditeit, Current ratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    # Current ratio = "B Vlottende activa" / "G Kortlopende schulden (ten hoogste 1 jaar)"
    totalen = {
        'B Vlottende activa': 0,
        'B.I Voorraden': 0,
        'B.II Vorderingen': 0,
        'B.III Effecten': 0,
        'B.IV Liquide middelen': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
        if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:1] == '3':
                totalen['B.I Voorraden'] += post.price
            elif rubriek_id[:2] == '12':
                totalen['B.II Vorderingen'] += post.price
            elif rubriek_id[:3] == '131':
                totalen['B.III Effecten'] += post.price
            elif rubriek_id[:3] == '132':
                totalen['B.IV Liquide middelen'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
    
    totalen['B Vlottende activa'] = (totalen['B.I Voorraden'] + 
        totalen['B.II Vorderingen'] + 
        totalen['B.III Effecten'] +
        totalen['B.IV Liquide middelen'] )
    
    if totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] != 0:
        waarde = (totalen['B Vlottende activa'] / totalen['G Kortlopende schulden (ten hoogste 1 jaar)'])
    else:
        waarde = 'ongedefineerd'
    output_string += '\n    Kental: Current ratio'
    output_string += f'\n    Waarde: {round(waarde, 4)}'
    output_string += f'\n    B Vlottende activa: {totalen["B Vlottende activa"]}'
    output_string += f'\n    G Kortlopende schulden (ten hoogste 1 jaar): {totalen["G Kortlopende schulden (ten hoogste 1 jaar)"]}'
    
    return (output_string)

def indicator_zk_19_6(start_periode, eind_periode, test_instances):
    output_string = f'\n19.6 - Liquiditeit, EBITDA (Earnings Before Interest, Tax, Deprecation and Amortisation), start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
    
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
        
    for jaarrekeningpost in ['P Som der bedrijfsopbrengsten', 'Q Som der bedrijfslasten', 'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    # EBITDA = "P Som der bedrijfsopbrengsten" - "Q Som der bedrijfslasten" + "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"
    ebitda = totalen['P Som der bedrijfsopbrengsten'] - totalen['Q Som der bedrijfslasten'] + totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']
    output_string += f'\n    EBITDA: {round(ebitda,2)}'
    return (output_string)

def indicator_zk_19_7(start_periode, eind_periode, test_instances):
    output_string = f'\n19.7 - Liquiditeit, EBITDA-ratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'P.I Netto omzet': 0,
        'P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum': 0,
        'P.III Geactiveerde productie voor het eigen bedrijf': 0,
        'P.IV Overige bedrijfsopbrengsten': 0,
        'P Som der bedrijfsopbrengsten': 0,
        'Q.I Kosten van grond- en hulpstoffen': 0,
        'Q.II Kosten uitbesteed werk en andere externe kosten': 0,
        'Q.III Lonen en salarissen': 0,
        'Q.IV Sociale lasten': 0,
        'Q.V Pensioenlasten': 0,
        'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa': 0,
        'Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa': 0,
        'Q.VIII Bijzondere waardevermindering van vlottende activa': 0,
        'Q.IX Overige bedrijfskosten': 0,
        'Q Som der bedrijfslasten': 0,
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:2] == '81':
                totalen['P.I Netto omzet'] += post.price
            elif rubriek_id[:2] in ['82', '83', '89'] or rubriek_id[:3] in ['919', '920', '930']:
                totalen['P.IV Overige bedrijfsopbrengsten'] += post.price
            elif rubriek_id[:3] in ['417', '418']:
                totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] += post.price
            elif rubriek_id[:3] in ['411', '412', '413', '414', '415', '416', '419']:
                totalen['Q.III Lonen en salarissen'] += post.price
            elif rubriek_id[:3] == '420' or rubriek_id[:4] in ['4221', '4223', '4224', '4225', '4229']:
                totalen['Q.IV Sociale lasten'] += post.price
            elif rubriek_id[:4] == '4226':
                totalen['Q.V Pensioenlasten'] += post.price
            elif rubriek_id[:3] in ['480', '481', '482', '483', '484']:
                totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] += post.price
            elif (rubriek_id[:3] in 
                ['423', '486', '489', '905', '911', '912', '913', '914', '915', '921', '931'] or 
                rubriek_id[:2] in ['43', '44', '45', '46', '47']):
                totalen['Q.IX Overige bedrijfskosten'] += post.price
    
    totalen['P Som der bedrijfsopbrengsten'] = (totalen['P.I Netto omzet'] + 
        totalen['P.II Wijziging in voorraden gereed product en onderhanden werk ten opzichte van de voorafgaande balansdatum'] + 
        totalen['P.III Geactiveerde productie voor het eigen bedrijf'] +
        totalen['P.IV Overige bedrijfsopbrengsten'] )
    totalen['Q Som der bedrijfslasten'] = (totalen['Q.I Kosten van grond- en hulpstoffen'] +
        totalen['Q.II Kosten uitbesteed werk en andere externe kosten'] +
        totalen['Q.III Lonen en salarissen'] +
        totalen['Q.IV Sociale lasten'] +
        totalen['Q.V Pensioenlasten'] +
        totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VII Overige waardevermindering immateriële vaste activa en materiële vaste activa'] +
        totalen['Q.VIII Bijzondere waardevermindering van vlottende activa'] +
        totalen['Q.IX Overige bedrijfskosten'] )
        
    for jaarrekeningpost in ['P Som der bedrijfsopbrengsten', 'Q Som der bedrijfslasten', 'Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    # EBITDA = "P Som der bedrijfsopbrengsten" - "Q Som der bedrijfslasten" + "Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa"
    ebitda = totalen['P Som der bedrijfsopbrengsten'] - totalen['Q Som der bedrijfslasten'] + totalen['Q.VI Afschrijvingen op immateriële vaste activa en materiële vaste activa']
    # EBITDA-ratio = EBITDA [zie Indicator 19.6] / "P Som der bedrijfsopbrengsten"
    ebitda_ratio = ebitda / totalen['P Som der bedrijfsopbrengsten']
    output_string += f'\n    EBITDA: {round(ebitda_ratio,2)}'
    return (output_string)

def indicator_zk_20_3(start_periode, eind_periode, test_instances):
    output_string = f'\n20.3 - Solvabiliteit, Debt ratio, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D Eigen vermogen': 0,
        'D.I Gestort en opgevraagd kapitaal': 0,
        'D.II Agio': 0,
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0,
        'E Voorzieningen': 0,
        'F Langlopende schulden (nog voor meer dan een jaar)': 0,
        'G Kortlopende schulden (ten hoogste 1 jaar)': 0,
        'H Totaal passiva': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            if rubriek_id[:4] == '0511':
                totalen['D.I Gestort en opgevraagd kapitaal'] += post.price
            elif rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
            elif rubriek_id[:2] == '06':
                totalen['E Voorzieningen'] += post.price
            elif rubriek_id[:2] == '07':
                totalen['F Langlopende schulden (nog voor meer dan een jaar)'] += post.price
            elif rubriek_id[:2] in ['14', '15']:
                totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] += post.price
    
    totalen['D Eigen vermogen'] =(totalen['D.I Gestort en opgevraagd kapitaal'] + 
        totalen['D.II Agio'] + 
        totalen['D.III Herwaarderingsreserve'] + 
        totalen['D.IV Wettelijke en statutaire reserve'] +
        totalen['D.V Bestemmingsreserve'] + 
        totalen['D.VI Bestemmingsfonds'] +
        totalen['D.VII Overige reserves'] + 
        totalen['D.VIII Onverdeelde winst'] )
    totalen['H Totaal passiva'] =(totalen['D Eigen vermogen'] + 
        totalen['E Voorzieningen'] + 
        totalen['F Langlopende schulden (nog voor meer dan een jaar)'] +
        totalen['G Kortlopende schulden (ten hoogste 1 jaar)'] )
        
    for jaarrekeningpost in ['F Langlopende schulden (nog voor meer dan een jaar)', 'G Kortlopende schulden (ten hoogste 1 jaar)', 'H Totaal passiva']:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
    
    if totalen['H Totaal passiva'] == 0:
        output_string += f'\n    Debt ratio: undefined'
    else:
        # Debt ratio = ("F Langlopende schulden (nog voor meer dan een jaar)" + "G Kortlopende schulden (ten hoogste 1 jaar)") / ("H Totaal passiva")
        result = (totalen['F Langlopende schulden (nog voor meer dan een jaar)'] + totalen['G Kortlopende schulden (ten hoogste 1 jaar)']) / totalen['H Totaal passiva']
        output_string += f'\n    Debt ratio: {round(result, 2)}'

    return output_string

def indicator_zk_20_4(start_periode, eind_periode, test_instances):
    output_string = f'\n20.4 - Solvabiliteit, Reserves, start_periode: {start_periode}, eind_periode: {eind_periode}'
    totalen = {
        'D.III Herwaarderingsreserve': 0,
        'D.IV Wettelijke en statutaire reserve': 0,
        'D.V Bestemmingsreserve': 0,
        'D.VI Bestemmingsfonds': 0,
        'D.VII Overige reserves': 0,
        'D.VIII Onverdeelde winst': 0
    }
    for post in [instance for instance in test_instances if isinstance(instance, Grootboekpost)]:
         if start_periode <= post.datum <= eind_periode :
            rubriek_id = str(post.rubriek).split('onz-fin#')[1]
            # D.III leeg
            if rubriek_id[:5] == '05413':
                totalen['D.IV Wettelijke en statutaire reserve'] += post.price
            elif rubriek_id[:5] in ['05311', '05342', '05423']:
                totalen['D.V Bestemmingsreserve'] += post.price
            elif rubriek_id[:5] == '05323':
                totalen['D.VI Bestemmingsfonds'] += post.price
            elif rubriek_id[:4] =='0539' or rubriek_id[:5] in ['05493', '05915']:
                totalen['D.VII Overige reserves'] += post.price
            elif rubriek_id[:3] == '091':
                totalen['D.VIII Onverdeelde winst'] += post.price
    result = 0
    for jaarrekeningpost in totalen:
        bedrag = totalen[jaarrekeningpost]
        output_string += f'\n    {jaarrekeningpost}: {round(bedrag,2)}'
        result += bedrag
    
    # Reserves = "D.III Herwaarderingsreserve" + "D.IV Wettelijke en statutaire reserve" + 
    # "D.V Bestemmingsreserve" + "D.VI Bestemmingsfonds" + "D.VII Overige reserves" + 
    # "D.VIII Onverdeelde winst"
    output_string += f'\n    Reserves: {round(result, 2)}'
        
    return output_string
