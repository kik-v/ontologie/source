from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, DC
from variables import *
from functions import *
from collections import defaultdict
from dataclasses import dataclass, field
from typing import List, Optional, Union, Tuple
from datetime import date

@dataclass
class Declaratie:
    subject: URIRef
    zorg_proces: URIRef
    goedgekeurd: bool
    product_code: str
    prestatie_code: str
    datum: date
    bedrag: float

    def to_rdf(self):
        g = Graph()
        g.add((self.subject, RDF.type, onzg.CareInvoice))
        g.add((self.subject, onzg.isAbout, self.zorg_proces))
        g.add((self.subject, onzg.goedgekeurd, Literal(self.goedgekeurd, datatype=XSD.boolean)))
        if self.prestatie_code:
            g.add((self.subject, onzfin.heeftZorgPrestatieCode, Literal(self.prestatie_code, datatype=XSD.string)))
        if self.product_code:
            g.add((self.subject, onzfin.heeftZorgProductCode, Literal(self.product_code, datatype=XSD.string)))
        g.add((self.subject, onzg.hasDate, Literal(self.datum, datatype=XSD.date)))
        financial_entity = BNode()
        financial_quality = BNode()
        financial_quality_value = BNode()
        g.add((self.subject, onzg.isAbout, financial_entity))
        g.add((financial_entity, RDF.type, onzg.FinancialEntity))
        g.add((financial_entity, onzg.hasQuality, financial_quality))
        g.add((financial_quality, onzg.hasQualityValue, financial_quality_value))
        g.add((financial_quality_value, onzg.hasDataValue, Literal(self.bedrag, datatype=XSD.decimal)))
        return g

@dataclass
class Grootboekpost:
    subject: URIRef
    datum: date
    price: float
    rubriek: URIRef
    
    def to_rdf(self):
        g = Graph()
        g.add((self.subject, RDF.type, onzfin.Grootboekpost))
        g.add((self.subject, onzg.partOf, self.rubriek))
        g.add((self.subject, onzg.hasDate, Literal(self.datum, datatype=XSD.date)))
        g.add((self.subject, onzfin.heeftGeldBedrag, Literal(self.price, datatype=XSD.decimal)))
        financial_entity = BNode()
        quality = BNode()
        quality_value = BNode()
        g.add((self.subject, onzg.isAbout, financial_entity))
        g.add((financial_entity, RDF.type, onzg.FinancialEntity))
        g.add((financial_entity, onzg.hasQuality, quality))
        g.add((quality, onzg.hasQualityValue, quality_value))
        g.add((quality_value, onzg.hasDataValue, Literal(self.price, datatype=XSD.decimal)))
        
        return g

@dataclass
class VerloondePeriode:
    subject: URIRef
    overeenkomst: object
    start_datum: date
    eind_datum: date
    aantal_uren: float

    def __post_init__(self):
        if not self.subject:
            self.subject = BNode().skolemize(basepath=dummy.VerloondePeriode_)

    def to_rdf(self):
        g = Graph()
        g.add((self.subject, RDF.type, onzfin.VerloondePeriode))
        g.add((self.subject, onzg.definedBy, self.overeenkomst.get_subject()))
        g.add((self.subject, onzg.startDatum, Literal(self.start_datum, datatype=XSD.date)))
        g.add((self.subject, onzg.eindDatum, Literal(self.eind_datum, datatype=XSD.date)))
        quality = BNode()
        g.add((self.subject, onzg.hasQuality, quality))
        quality_value = BNode()
        g.add((quality, onzg.hasQualityValue, quality_value))
        g.add((quality_value, onzg.hasUnitOfMeasure, onzg.Uur))
        g.add((quality_value, onzg.hasDataValue, Literal(self.aantal_uren, datatype=XSD.decimal)))

        return g