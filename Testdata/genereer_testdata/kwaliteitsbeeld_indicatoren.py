from variables import *
from functions import *
from rdf_client_objecten import *
from rdf_fin_objecten import *

def indicator_vb_1(mensen):
    unieke_mensen = []
    leeftijden = []
    periode = (date(2024, 1, 1), date(2024, 12, 31))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten: #persoon is dus in loondienst
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    if period1_inperiod2(contract_periode, periode):
                        geboorte_datum = mens.get_geboortedatum()
                        # Gebruik eind van de periode om de leeftijd te berekenen
                        leeftijd = periode[1].year - geboorte_datum.year
                        if mens not in unieke_mensen:
                            leeftijden.append(leeftijd)
                            unieke_mensen.append(mens)
    start = 16
    end = 75
    interval = 5
    intervals = list(range(start, end + 1, interval))
    indicator = {}
    for i in range(len(intervals) - 1):
        lower = intervals[i]
        upper = intervals[i + 1]
        key = f"{lower}-{upper-1}"
        
        # Count numbers in this interval
        freq = sum(1 for num in leeftijden if lower <= num < upper)
        indicator[key] = freq
    
    # Explicitly add the last interval
    last_interval_key = f"{intervals[-1]}-{end}"
    last_interval_freq = sum(1 for num in leeftijden if intervals[-1] <= num <= end)
    indicator[last_interval_key] = last_interval_freq
    
    output_string = f"\nVoorbeeld 1:\n"
    for key, value in indicator.items():
        output_string += f"{key} jaar: {value}, "
    return (output_string)

def indicator_vb_2(peildatum, mensen):
    output_string = f'\nVoorbeeld 2, peildatum {peildatum}:'
    indicator = {}
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            type_overeenkomst = werkovereenkomst.get_rdftype().split('/')[-1]
            for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                contract_periode = werkovereenkomstafspraak.get_periode()
                if date_in_period(peildatum, contract_periode):
                    functie = werkovereenkomstafspraak.get_functie()
                    niveau = FunctieNiveaus.get(functie)
                    if niveau:
                        if niveau not in indicator:
                            indicator[niveau] = {}
                            indicator[niveau]["Totaal"] = 0
                        if type_overeenkomst not in indicator[niveau]:
                            indicator[niveau][type_overeenkomst] = 0
                        indicator[niveau][type_overeenkomst] += 1
                        indicator[niveau]["Totaal"] += 1
                            
    sorted_dict = {key: indicator[key] for key in sorted(indicator.keys())}
    for niveau in sorted_dict:
        output_string += '\n    ' + str(niveau.split('/')[-1]) + ':'
        for type_overeenkomst in indicator[niveau]:
            output_string += ' ' + type_overeenkomst.split('#')[-1] + ': ' + str(indicator[niveau][type_overeenkomst])
    return output_string

def indicator_vb_3(mensen):
    periode = (date(2023, 1, 1), date(2023, 1, 31))
    indicator = {}
    indicator["Totaal"] = {}
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            werkovereenkomst_type = werkovereenkomst.get_rdftype()
            if True: #werkovereenkomst_type in ArbeidsOvereenkomsten:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for gewerkte_periode in gewerkte_perioden:
                    dag = gewerkte_periode.get_date()
                    if date_in_period(date=dag, period=periode): #gewerkte periode in meetperiode
                        werkovereenkomstafspraak = get_afspraak_on_date(werk_overeenkomst=werkovereenkomst, date=dag)
                        functie = werkovereenkomstafspraak.get_functie()
                        niveau = FunctieNiveaus.get(functie)
                        if niveau:
                            functie_niveau = niveau.split('#')[1]
                            overeenkomst_type = werkovereenkomst.get_rdftype().split('#')[1]
                            uren = gewerkte_periode.get_uren()
                            if functie_niveau not in indicator:
                                indicator[functie_niveau]={}
                            if overeenkomst_type not in indicator[functie_niveau]:
                                indicator[functie_niveau][overeenkomst_type] = 0
                            if overeenkomst_type not in indicator["Totaal"]:
                                indicator["Totaal"][overeenkomst_type] = 0    
                            indicator[functie_niveau][overeenkomst_type] += uren
                            indicator["Totaal"][overeenkomst_type] += uren
    sorted_dict = {key: indicator[key] for key in sorted(indicator.keys())}
    output_string = f"\nVoorbeeld 3:\n"
    for niveau in sorted_dict:
        output_string += f"{niveau}: {sorted_dict[niveau]}\n"
    return output_string

def indicator_vb_4(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nVoorbeeld 4, peildatum: {peildatum}"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        client = zorg_proces.deelnemer.subject
        if date_in_period(peildatum, zorg_periode):
            indicatie = zorg_proces.indicatie
            type_indicatie = indicatie.rdf_type
            if type_indicatie == onzzorg.WlzIndicatie:
                langzorgsector = False
                if indicatie.zorgprofiel in ggzb_zorgprofielen:
                    langzorgsector = URIRef("http://purl.org/ozo/onz-zorg#GGZ-B")
                elif indicatie.zorgprofiel in ggzw_zorgprofielen:
                    langzorgsector = URIRef("http://purl.org/ozo/onz-zorg#GGZ-W")
                elif indicatie.zorgprofiel in lg_zorgprofielen:
                    langzorgsector = onzzorg.LG
                elif indicatie.zorgprofiel in lvg_zorgprofielen:
                    langzorgsector = onzzorg.LVG
                elif indicatie.zorgprofiel in vv_zorgprofielen:
                    langzorgsector = onzzorg.VV
                elif indicatie.zorgprofiel in vg_zorgprofielen:
                    langzorgsector = onzzorg.VG
                elif indicatie.zorgprofiel in zgaud_zorgprofielen:
                    langzorgsector = onzzorg.ZGAUD
                elif indicatie.zorgprofiel in zgvis_zorgprofielen:
                    langzorgsector = onzzorg.ZGVIS
                if langzorgsector != False:
                    if langzorgsector not in unique_clients:
                        unique_clients[langzorgsector] = {}
                        unique_clients[langzorgsector]['totaal'] = 0
                        unique_clients[langzorgsector]['clienten'] = []
                    if client not in unique_clients[langzorgsector]['clienten']:
                        unique_clients[langzorgsector]['clienten'].append(client)
                        unique_clients[langzorgsector]['totaal'] += 1
    output_string = f"\nVoorbeeld 4:\n"
    for langzorgsector in unique_clients.keys():
        # value = unique_clients[langzorgsector]['totaal']
        output_string += f'{langzorgsector.split("#")[1]}: {unique_clients[langzorgsector]["totaal"]}, '
    return (output_string)

def indicator_vb_5(peildatum, test_instances):
    unique_clients = {}
    output_string = f"\nVoorbeeld 5, peildatum: {peildatum}\n"
    for zorg_proces in [instance for instance in test_instances if isinstance(instance, ZorgProces)]:
        zorg_periode = zorg_proces.periode
        client = zorg_proces.deelnemer.subject
        if date_in_period(peildatum, zorg_periode):
            indicatie = zorg_proces.indicatie
            type_indicatie = indicatie.rdf_type
            if type_indicatie == onzzorg.WlzIndicatie:
                if indicatie.zorgprofiel in vv_zorgprofielen:
                    lv = indicatie.leveringsvorm
                    if indicatie.leveringsvorm not in unique_clients:
                        unique_clients[lv] = {}
                        unique_clients[lv]['totaal'] = 0
                        unique_clients[lv]['clienten'] = []
                    if client not in unique_clients[lv]['clienten']:
                        unique_clients[lv]['clienten'].append(client)
                        unique_clients[lv]['totaal'] += 1
    for leveringsvorm in unique_clients.keys():
        output_string += f'{leveringsvorm.split("#")[1]}: {unique_clients[leveringsvorm]["totaal"]}, '
    return output_string

def indicator_vb_6(jaar, mensen):
    indicator = {"teller": 0, "noemer": 0}
    periode = (date(jaar, 1, 1), date(jaar, 12, 31))
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    afspraak_periode = overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode)
                    if afspraak_periode:
                        contract_omvang = werkovereenkomstafspraak.get_contractomvang()
                        ptf = contract_omvang.get_waarde()
                        omvang_eenheid = contract_omvang.get_eenheid()
                        if omvang_eenheid == onzpers.Uren_per_week_unit:
                            ptf = ptf / 36 # omvang in fte
                        functie = werkovereenkomstafspraak.get_functie()
                        if functie in ZorgFuncties:
                            #locatie = werkovereenkomstafspraak.get_werklocatie().split("/")[-1]
                            dagen_afspraak = days_in_period(overlap_with_fixed_period(werkovereenkomstafspraak.get_periode(), periode))
                            indicator["noemer"] += dagen_afspraak * ptf
                            for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                                verzuim_periode = overlap_with_fixed_period(verzuim.get_periode(), afspraak_periode)
                                #Ziekte en/of Zwangerschap
                                if verzuim_periode and verzuim.get_type() == onzpers.ZiektePeriode:
                                    totaal_verzuim_dagen = days_in_period(overlap_with_fixed_period(verzuim.get_periode(), periode))
                                    hersteld = 0
                                    verzuim_corr = days_in_period(verzuim_periode)
                                    for verzuimtijd in verzuim.get_verzuimtijd_lijst():
                                        verzuimtijd_periode = overlap_with_fixed_period(verzuimtijd.get_periode(), verzuim_periode)
                                        if verzuimtijd_periode:
                                            verzuimtijd_dagen = days_in_period(verzuimtijd_periode)
                                            vp = verzuimtijd.get_ziekte_percentage() / 100
                                            hersteld += verzuimtijd_dagen * (1-vp)
                                    indicator["teller"] += ptf * (verzuim_corr - hersteld)

    output_string = f"\nKB vorbeeld 6 indicator jaar: {jaar}:\n"
    zorg_indicator = str(100 * indicator["teller"]/indicator["noemer"]) if indicator["noemer"] != 0 else "Ongedefinieerd"
    output_string += "\tteller: " + str(indicator["teller"])
    output_string += ", noemer: " + str(indicator["noemer"])
    output_string += ", indicator: " + str(zorg_indicator)
    # print(output_string)
    return output_string

def indicator_vb_7(jaar, mensen):
    periode = (date(jaar, 1, 1), date(jaar, 12, 31))
    personen = set()
    verzuim = set()
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten: #persoon is dus in loondienst
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    contract_periode = werkovereenkomstafspraak.get_periode()
                    functie = werkovereenkomstafspraak.get_functie()
                    if period1_inperiod2(contract_periode, periode) and functie in ZorgFuncties:
                        personen.add(mens.get_subject())
                        verzuim_perioden = werkovereenkomst.get_verzuim_periode_lijst()
                        for verzuim_periode in verzuim_perioden:
                            start_verzuim = verzuim_periode.get_periode()[0]
                            if date_in_period(start_verzuim, contract_periode) and date_in_period(start_verzuim, periode):
                                verzuim.add(verzuim_periode.get_subject())
    # print(len(personen))
    # print(len(verzuim))
    output_string = f"\nVoorbeeld 7, jaar: {jaar}\n"
    output_string += f"    teller: {len(verzuim)}, noemer: {len(personen)}, indicator: {len(verzuim)/len(personen)}"
    return (output_string)
