import random
import pickle
import variables
import rdf_objecten
from rdflib import Graph, URIRef, Literal, RDF, Namespace
from datetime import datetime, timedelta
from rdf_objecten import *
from variables import *
from functions import *


test_instances = []
periode = (timeframe_start, timeframe_end)
meetperiode = (query_start, query_end)
pickle_filename = 'generated_testdata.pkl'
turtle_filename = 'generated_testdata.ttl'

def create_test_data():
    instances = []
    for i in range(num_people):
        m = Mens()
        start_contract = random_date_within_period(periode)
        for sub_period in split_periods((start_contract, periode[1])):
            wo = WerkOvereenkomst(m, sub_period)
            m.append_werkovereenkomst(wo)
            # genereer zwangerschapsverlof (onafhakelijk van ziekte; kan dus naast elkaar bestaan)
            if random.random() < 0.05: #kans van 20% per werkovereenkomst op zwangerschap
                zw_verlof_start = random_date_within_period(sub_period)
                zw_verlof_eind = zw_verlof_start + timedelta(weeks=12)
                if zw_verlof_eind > periode[1]: #verlof kan niet langer duren dan contract
                    zw_verlof_eind = periode[1]
                zw_verlof = VerzuimPeriode(wo, periode=(zw_verlof_start, zw_verlof_eind), type=onzpers.ZwangerschapsVerlof)
                if random.random() < 0.1:
                    zw_vt = VerzuimTijd(verzuim_periode=zw_verlof, periode=(zw_verlof_start, zw_verlof_eind), ziekte_percentage=100)
                    zw_verlof.append_verzuimtijd(zw_vt)
                    instances.append(zw_vt)
                wo.append_verzuim_periode(zw_verlof)
                instances.append(zw_verlof)
            # generate ziekte
            if period1_inperiod2(sub_period, meetperiode):
                current_day = sub_period[0] if sub_period[0] > meetperiode[0] else meetperiode[0] - timedelta(weeks=12)
                end_day = sub_period[1] if sub_period[1] < meetperiode[1] else meetperiode[1]
                while current_day <= end_day:
                    if random.random() < 0.01 : # 1% chance of sickness
                        sickness_period, sickness_sub_periods = generate_sickness_period(current_day)
                        vp = VerzuimPeriode(wo, sickness_period, type=onzpers.ZiektePeriode)
                        wo.append_verzuim_periode(vp)
                        for sickness_sub_period in sickness_sub_periods:
                            vt = VerzuimTijd(vp, sickness_sub_period, random.choice(ziekte_percentages))
                            vp.append_verzuimtijd(vt)
                            instances.append(vt)
                        instances.append(vp)
                    current_day += timedelta(days=1)
            # genereer werkafspraken
            for sub_sub_period in split_periods(sub_period):
                omvangwaarde = 0
                woa = WerkOvereenkomstAfspraak(wo, sub_sub_period)
                wo.append_werkovereenkomstafspraak(woa)
                co = ContractOmvang()
                woa.add_contractomvang(co)
                instances.append(co)
                instances.append(woa)
                # uren registratie per werkafspraak
                current_day = sub_sub_period[0]
                end_day = sub_sub_period[1] if sub_sub_period[1] else query_end # if no enddate then take query end
                while current_day <= end_day and current_day <= query_end:
                    if current_day >= query_start and current_day <= query_end : #only generate within query time
                        omvangwaarde = co.get_waarde() * 36 if co.get_eenheid() == onzpers.fte_36 else co.get_waarde()
                        if random.random()*omvangwaarde/36 < 5/7: #chance depends on contractomvang
                            gw = GewerktePeriode(wo, current_day)
                            wo.append_gewerkte_periode(gw)
                            gt = GewerkteTijd(gw)
                            gw.set_gewerktetijd(gt)
                            instances.append(gw)
                            instances.append(gt)
                    current_day += timedelta(days=10) # only 1/10th of days to limit amout of testdata
            instances.append(wo)
        instances.append(m)
    return(instances)

def create_rdf_from(instances):
    g = Graph()
    g.bind('onz-org', onzorg)
    g.bind('onz-g', onzg)
    g.bind('onz-pers', onzpers)
    g.bind('dummy', dummy)
    for instance in instances:
        g += instance.to_rdf()
    return g    

def save_pickle_file_from(instances, filename):
    with open(filename, 'wb') as file:
        for instance in instances:
            pickle.dump(instance, file)

print("Creating testdata....")
test_instances = create_test_data()
print(f"Saving {len(test_instances)} instances of testdata to {pickle_filename}")
save_pickle_file_from(test_instances, pickle_filename)

print("Creating RDF graph")
g = create_rdf_from(test_instances)

print(f"Created graph wiht {len(g)} triples")
print(f"Serializing graph to {turtle_filename}")
g.serialize(destination=turtle_filename, format='turtle')
