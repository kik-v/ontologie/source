import random
import pickle
import variables
from rdf_client_objecten import *
from rdf_fin_objecten import *
from rdflib import Graph, URIRef, Literal, RDF, Namespace
from datetime import datetime, timedelta
from rdf_objecten import *
from variables import *
from functions import *

def create_declaraties(zorg_proces: ZorgProces) -> list:
    declaraties = []
    start_date, end_date = zorg_proces.periode
    if not end_date:
        end_date = timeframe_end
    # limit period to generate declartions to time_frame
    periode = trim_to_period((start_date, end_date), (timeframe_start, timeframe_end))
    if periode:
        # generate one declaration per month
        random_dates = generate_dates_in_period(periode, months_in_period(periode)) 
        for datum in random_dates:
            # indicatie_type = str(zorg_proces.indicatie.rdf_type).split('#')[1]
            # aanname: geen probleem als de declaratiecode niet bij de indicatie hoort
            heeft_product_code = random.random() < 0.5
            declaratie = Declaratie(
                subject= BNode().skolemize(basepath=dummy.Declaratie_),
                zorg_proces= zorg_proces.subject,
                goedgekeurd= True if random.random() < 0.95 else False, # 95% of all declarations is approved
                product_code= random.choice(ProductCodes) if heeft_product_code else None,
                prestatie_code= random.choice(PrestatieCodes_flat) if not heeft_product_code else None,
                datum= datum,
                bedrag= round(random.random() * 1000.00, 2)
            )
            declaraties.append(declaratie)
    return declaraties

def create_grootboekposts() -> list:
    grootboekposts = []
    # limit period to generate declartions to time_frame
    periode = (timeframe_start, timeframe_end)
    alle_rubrieken =  rgs_rubrieken + prismant_rubrieken
    
    # generate five grootboekposts per month
    random_dates = generate_dates_in_period(periode, 5 * months_in_period(periode)) 
    for datum in random_dates:
        grootboekpost = Grootboekpost(
            subject= BNode().skolemize(basepath=dummy.Grootboekpost_),
            rubriek= random.choice(alle_rubrieken),
            datum= datum,
            price= round(random.random() * 1000.00, 2)
        )
        grootboekposts.append(grootboekpost)

    # also generate 1 grootboekpost per rubriek
    random_dates_per_rubriek = generate_dates_in_period(periode, len(alle_rubrieken))
    for (datum, rubriek) in zip(random_dates_per_rubriek, alle_rubrieken):
        grootboekpost = Grootboekpost(
            subject= BNode().skolemize(basepath=dummy.Grootboekpost_),
            rubriek= rubriek,
            datum= datum,
            price= round(random.random() * 1000.00, 2)
        )
        grootboekposts.append(grootboekpost)

    return grootboekposts


def create_verloonde_perioden(gewerkte_perioden) -> list:
    verloonde_perioden = []
    # maak voor 95% van de gewerkte perioden een verloonde periode aan
    # vat alle gewerkte perioden per werkafspraak per maand samen en maakt voor het totaal een verloonde periode aan
    gewerkte_perioden.sort(key=lambda x: (x.get_overeenkomst().get_subject(), x.get_date())) # sorteer op werkovereenkomst en datum
    start_overeenkomst = gewerkte_perioden[0].get_overeenkomst()
    start_datum_periode = gewerkte_perioden[0].get_date()
    eind_datum_periode = start_datum_periode
    start_afspraak = get_afspraak_on_date(werk_overeenkomst=start_overeenkomst, date=start_datum_periode)
    verloonde_uren = 0
    for gewerkte_periode in gewerkte_perioden:
        overeenkomst = gewerkte_periode.get_overeenkomst()
        datum_periode = gewerkte_periode.get_date()
        afspraak = get_afspraak_on_date(werk_overeenkomst=overeenkomst, date=datum_periode)
        uren = gewerkte_periode.get_uren()
        if afspraak.get_subject() != start_afspraak.get_subject() or (start_datum_periode.year, start_datum_periode.month) != (datum_periode.year, datum_periode.month):
            verloonde_periode = VerloondePeriode(
                subject=None,
                overeenkomst=start_overeenkomst,
                start_datum=start_datum_periode,
                eind_datum=eind_datum_periode,
                aantal_uren=verloonde_uren
            )
            verloonde_perioden.append(verloonde_periode)
            # print(verloonde_periode.subject, start_datum_periode, eind_datum_periode, verloonde_uren)
            # print('     ' , afspraak.get_subject(), datum_periode, uren)
            # start overnieuw voor nieuwe verloonde periode
            start_datum_periode = datum_periode
            start_afspraak = afspraak
            start_overeenkomst = overeenkomst
            verloonde_uren = 0
        # else:
        #     print('     ' , afspraak.get_subject(), datum_periode, uren)
        
        if random.random() < 0.95:
            verloonde_uren += uren
        eind_datum_periode = datum_periode

    # voor laatste periode
    verloonde_periode = VerloondePeriode(
            subject=None,
            overeenkomst=start_overeenkomst,
            start_datum=start_datum_periode,
            eind_datum=eind_datum_periode,
            aantal_uren=verloonde_uren
        )
    verloonde_perioden.append(verloonde_periode)
    # print(verloonde_periode.subject, start_datum_periode, eind_datum_periode, verloonde_uren)
    return verloonde_perioden

    

client_instances = load_test_data(client_data_file)
zorgprocessen = get_objects_from(client_instances, ZorgProces)
indicaties = get_objects_from(client_instances, Indicatie)
test_data = load_test_data(test_data_file)
gewerkte_perioden = get_objects_from(test_data, GewerktePeriode)

instances = []
print("Creating testdata....")

for zorg_proces in zorgprocessen:
    instances += create_declaraties(zorg_proces)

instances += create_grootboekposts()

verloonde_perioden = create_verloonde_perioden(gewerkte_perioden=gewerkte_perioden)
instances += verloonde_perioden

print(f"Saving {len(instances)} instances of testdata to {fin_data_file}")
save_pickle_file_from(instances, fin_data_file)

print("Creating RDF graph")
g = create_rdf_from(instances)

print(f"Created graph with {len(g)} triples")
print(f"Serializing graph to {fin_data_turtle}")
g.serialize(destination=fin_data_turtle, format='turtle')
