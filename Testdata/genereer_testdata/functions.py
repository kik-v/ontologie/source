import random
import pickle
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, DC
from datetime import datetime, timedelta, date, time
from dateutil.relativedelta import relativedelta
from variables import *
import rdf_objecten

def generate_random_birthdate(min_age=18, max_age=65):
    # Calculate the date range for the birthdate
    today = datetime.today()
    start_date = today - timedelta(days=max_age*365)  # 65 years ago
    end_date = today - timedelta(days=min_age*365)    # 18 years ago
    
    # Generate a random date within this range
    random_date = start_date + timedelta(days=random.randint(0, (end_date - start_date).days))
    
    # Return the random date
    return random_date.date()

def random_date_within_period(period):
    start_date, end_date = period
    delta = end_date - start_date
    random_days = random.randint(0, delta.days)
    return start_date + timedelta(days=random_days)

def split_periods(period, min_days=None, max_days=None):
    start_date, end_date_temp = period
    end_date = end_date_temp if end_date_temp else timeframe_end
    total_days = (end_date - start_date).days
    if not min_days:
        min_days = 200
    if not max_days:
        max_days = 250
    sub_periods = []
    current_start = start_date
    
    while current_start <= end_date:
        remaining_days = (end_date - current_start).days
        # Ensure the sub-periods are approximately 100 days but can vary slightly
        sub_period_length = min(random.randint(min_days, max_days), remaining_days)
        current_end = current_start + timedelta(days=sub_period_length)
        
        # Ensure the last period ends exactly at end_date
        if current_end > end_date:
            current_end = end_date
        
        sub_periods.append((current_start, current_end))
        current_start = current_end + timedelta(days=1) # Next period starts the day after the current ends
    
    if not end_date_temp: #remove last enddate
        sub_periods[len(sub_periods)-1] = (sub_periods[len(sub_periods)-1][0], None)

    return sub_periods

def period1_inperiod2(period1, period2):
    # Unpack the periods
    start1, end1 = period1
    start2, end2 = period2

    if not end2:
        end2 = datetime.today().date() + timedelta(days=36500)

    # Convert False to a date far in the future for comparison
    if not end1:
        end1 = end2
    return (start1 <= end2 and end1 >= start2)

def date_in_period(date, period):
    start, end_temp = period
    end = timeframe_end+timedelta(days = 36500) if not end_temp else end_temp
    return (start <= date <= end)

def trim_to_period(period1, period2):
    # get the smallast overlap of two periods. If end dates are missing, use meetperiode
    start1, end1 = period1
    start2, end2 = period2
    end1 is meetperiode[1] if not end1 else end1
    end2 is meetperiode[1] if not end2 else end2
    if period1_inperiod2(period1, period2):
        return ( start1 if start1 >= start2 else start2, end1 if end1 <= end2 else end2)
    else:
        return False

def overlap_with_fixed_period(period1, fixed_period):
    # Get the smallast overlap of two periods.
    # It is assumed that fixed_period always has an end date.
    start1, end1 = period1
    start2, end2 = fixed_period
    if end1 == None or not end1: 
        end1 = end2
    # if one of the periods starts in the span of the other, they overlap
    if (start1 <= start2 <= end1) or (start2 <= start1 <= end2):
        return (max(start1, start2), min(end1, end2))
    else:
        return False

def days_in_period(period):
    if period[0] and period[1]:
        return (period[1] - period[0]).days + 1
    else:
        return False

def months_in_period(period):
    start_date, end_date = period
    if start_date and end_date:
        return int((end_date - start_date).days/30)
    else:
        return False

def add_random_time_to_date(date_obj):
    # Generate random hours, minutes, and seconds
    random_hour = random.randint(0, 23)
    random_minute = random.randint(0, 59)
    random_second = random.randint(0, 59)
    
    # Create a time object with the random values
    random_time = time(random_hour, random_minute, random_second)
    
    # Combine the date object with the random time object to get a datetime object
    random_datetime = datetime.combine(date_obj, random_time)
    
    return random_datetime

def generate_sickness_period(start_date):
    num_days_sick = random.randint(1, 15)
    end_date = None if random.random() < 0.05 else start_date + timedelta(days = num_days_sick)
    period = (start_date, end_date)
    if end_date:
        sickness_periods = split_periods(period, 1, 7)
    else:
        sickness_periods = split_periods(period, 200,300)
    return((start_date, end_date), sickness_periods)

def iterate_over_period(period):
    start_date, end_date = period
    current_date = start_date
    while current_date <= end_date:
        yield current_date
        current_date += timedelta(days=1)

def load_test_data(pickle_file_path):
    with open(pickle_file_path, 'rb') as file:
        instances = []
        while True:
            try:
                # Load each instance and append to the list
                instance = pickle.load(file)
                instances.append(instance)
            except EOFError:
                # End of file reached
                break
    return instances

def get_people_from(instances):
    mensen = []
    for instance in instances:
        if type(instance) == rdf_objecten.Mens:
            mensen.append(instance)
    return mensen

def get_objects_from(instances:list, object_type) -> list:
    objects = []
    for instance in instances:
        if type(instance) == object_type:
            objects.append(instance)
    return objects

def create_rdf_from(instances):
    g = Graph()
    g.bind('onz-org', onzorg)
    g.bind('onz-zorg', onzzorg)
    g.bind('onz-g', onzg)
    g.bind('onz-pers', onzpers)
    g.bind('onz-fin', onzfin)
    g.bind('dummy', dummy)
    for instance in instances:
        g += instance.to_rdf()
    return g    

def save_pickle_file_from(instances, filename):
    with open(filename, 'wb') as file:
        for instance in instances:
            pickle.dump(instance, file)

def generate_dates_in_period(period, num_dates):
    """
    Generate a list of random dates within a given period.

    Parameters:
    period (start_date (datetime.date), end_date (datetime.date): start and end of the period.
    num_dates (int): The number of dates to generate.

    Returns:
    list: A list of random dates within the specified period.
    """
    start_date, end_date = period
    if not end_date:
        end_date = timeframe_end #if end date is missing, substitute eind of timeframe to generate data in
    delta = end_date - start_date
    random_dates = []

    for _ in range(num_dates):
        random_days = random.randint(0, delta.days)
        random_date = start_date + timedelta(days=random_days)
        random_dates.append(random_date)

    return random_dates

def last_day_of_month(year, month):
    # If month is 12, next month is 1 of next year
    if month == 12:
        next_month = 1
        next_year = year + 1
    else:
        next_month = month + 1
        next_year = year
    
    # Get first day of next month
    first_of_next_month = date(next_year, next_month, 1)
    
    # Subtract one day
    return first_of_next_month - timedelta(days=1)

def get_afspraak_on_date(werk_overeenkomst:object, date:date) -> object:
    afsprakenlijst = werk_overeenkomst.get_werkafspraken_lijst()
    for afspraak in afsprakenlijst:
        periode = afspraak.get_periode()
        if date_in_period(date=date, period=periode):
            return  afspraak
    return False

def is_care_on_date(werk_overeenkomst:object, date: date) -> bool:
    afspraak = get_afspraak_on_date(werk_overeenkomst=werk_overeenkomst, date=date)
    if afspraak:
        if afspraak.get_functie() in ZorgFuncties:
                return True
    return False


def expand_rubrieken(rubrieken):
    # Used for RGS rubrieken. Expands a set so that it also includes the children.
    old_rubrieken = rubrieken
    new_rubrieken = set()
    num_old_rubrieken, num_new_rubrieken = len(old_rubrieken), len(new_rubrieken)
    # print(f'Loop condition: checking num_old vs num_new: {num_old_rubrieken} vs {num_new_rubrieken}')
    while num_old_rubrieken != num_new_rubrieken:
        expansion = set()
        # if r is a parent in the hierarchy, add its children to the list.
        # otherwise just add r itself.
        for r in old_rubrieken:
            if r in rgs_hierarchy:
                expansion |= set(rgs_hierarchy[r])
            else:
                expansion |= set([r])
        
        old_rubrieken = new_rubrieken
        new_rubrieken |= set(expansion)
        num_old_rubrieken, num_new_rubrieken = len(old_rubrieken), len(new_rubrieken)
        # print(f'Loop condition: checking num_old vs num_new: {num_old_rubrieken} vs {num_new_rubrieken}')
    
    return new_rubrieken | set(rubrieken)
