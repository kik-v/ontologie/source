from variables import *
from functions import *
from zk_indicatoren import *
from igj_indicatoren import *
from kwaliteitsbeeld_indicatoren import *

pickle_filename = 'generated_testdata.pkl'
grootboek_pickle_filename = 'generated_fin_data.pkl'
clients_pickle_filename = 'generated_test_clients.pkl'
indicatoren_filename = 'indicatoren.txt'

test_instances = load_test_data(pickle_filename)
test_clients = load_test_data(clients_pickle_filename)
test_grootboek_instances = load_test_data(grootboek_pickle_filename)
mensen = get_people_from(test_instances)

with open (indicatoren_filename, 'w') as file:
    print("Writing calculations voor Kwaliteitsbeeld")
    file.write('\n\nIndicatoren Kwaliteitsbeeld: \n')
    file.write(indicator_vb_1(mensen) + '\n')
    file.write(indicator_vb_2(peildatum=date(2024,6,1), mensen=mensen) + '\n')
    file.write(indicator_vb_3(mensen))
    file.write(indicator_vb_4(peildatum=date(2024,12,31), test_instances=test_clients) + '\n')
    file.write(indicator_vb_5(peildatum=date(2024,12,31), test_instances=test_clients) + '\n')
    file.write(indicator_vb_6(jaar=2023, mensen=mensen) + '\n')
    file.write(indicator_vb_7(jaar=2023, mensen=mensen) + '\n')

    print("Writing calculations voor Zorgkantoren")
    file.write('\n\nIndicatoren Zorgkantoren: \n')
    file.write(indicator_zk_1_1(jaar=2023, kwartaal='Q2', mensen=mensen) + '\n')
    file.write(indicator_zk_1_2(date(2024,6,12), mensen) + '\n')
    file.write(indicator_zk_2_1(jaar=2023, kwartaal='Q4', mensen=mensen) + '\n')
    file.write(indicator_zk_2_2(jaar=2023, kwartaal="Q3", financial_instances=test_grootboek_instances, test_instances=test_instances) + '\n')
    file.write(indicator_zk_3_1(date(2023,9,12), mensen) + '\n')
    file.write(indicator_zk_4_1(jaar=2023, kwartaal='Q3', mensen=mensen))
    file.write(indicator_zk_5_1(jaar=2023, kwartaal='Q1', mensen=mensen))
    file.write(indicator_zk_6_1(jaar=2023, kwartaal='Q3', mensen=mensen))
    file.write(indicator_zk_7_1(jaar=2023, kwartaal='Q3', mensen=mensen, clienten=test_clients))
    file.write(indicator_zk_8_2(date(2020,1,1), date(2024,12,31), test_grootboek_instances) + '\n')
    file.write(indicator_zk_11_1(jaar=2023, kwartaal='Q3', mensen=mensen) + '\n')
    file.write(indicator_zk_11_2(jaar=2023, kwartaal='Q4', mensen=mensen) + '\n')
    file.write(indicator_zk_11_3(jaar=2023, kwartaal='Q2', mensen=mensen) + '\n')
    file.write(indicator_zk_11_4(jaar=2024, kwartaal='Q1', mensen=mensen) + '\n')
    file.write(indicator_zk_12_1(jaar=2023, kwartaal='Q1', mensen=mensen) + '\n')
    file.write(indicator_zk_12_2(jaar=2023, kwartaal='Q1', mensen=mensen) + '\n')
    file.write(indicator_zk_13_1(peildatum=date(2024,8,1), mensen=mensen) + '\n')
    file.write(indicator_zk_13_2(peildatum=date(2024,1,1), mensen=mensen) + '\n')
    file.write(indicator_zk_13_3(peildatum=date(2024,9,12), mensen=mensen) + '\n')
    file.write(indicator_zk_13_4(peildatum=date(2024,5,12), mensen=mensen) + '\n')
    file.write(indicator_zk_14_1(date(2024,1,1), test_clients) + '\n')
    file.write(indicator_zk_14_2(date(2023,1,1), test_clients) + '\n')
    file.write(indicator_zk_14_3(date(2023,1,1), test_clients) + '\n')
    file.write(indicator_zk_15_1(test_clients) + '\n')
    file.write(indicator_zk_15_2(test_clients) + '\n')
    file.write(indicator_zk_15_3(test_clients) + '\n')
    file.write(indicator_zk_15_4(date(2023,1,1), test_clients) + '\n')
    file.write(indicator_zk_18_1(date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_18_2(date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_19_2(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_19_3(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_19_4(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_19_5(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_19_6(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_19_7(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_20_3(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')
    file.write(indicator_zk_20_4(date(2020,1,1),date(2025,1,1), test_grootboek_instances) + '\n')

    print("Writing calculations voor IGJ")
    file.write('\n\nIndicatoren IGJ: \n')
    file.write(indicator_igj_1_2_1(peildatum=date(2023,5,19), vestiging=dummy.Vestiging_De_Beuk, mensen=mensen) + '\n')
    file.write(indicator_igj_1_2_3(peildatum=date(2023,5,19), vestiging=dummy.Vestiging_De_Beuk, mensen=mensen) + '\n')
    file.write(indicator_igj_1_4_1(peildatum=date(2023,8,12), vestiging=dummy.Vestiging_De_Beuk, mensen=mensen) + '\n')
    file.write(indicator_igj_1_4_2(peildatum=date(2023,9,15), vestiging=dummy.Vestiging_De_Beuk, test_instances=test_instances) + '\n')


print(f"Calculations saved to {indicatoren_filename}")
