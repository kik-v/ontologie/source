from rdf_client_objecten import *
from variables import *
from datetime import date
from functions import create_rdf_from, save_pickle_file_from

test_instances = []
g = Graph()

def generate_clients():
    instances = []
    previous_woonenheid = False
    for i in range(100):
        # genereer client
        client = Client(subject=BNode().skolemize(basepath=dummy.Client_))
        instances.append(client)

        # genereer indicatie
        indicatie_datum = meetperiode[1] - timedelta(days=int(random.random()*1500))
        if random.random() < 0.05: # kans van 5% dat de indicatie een einddatum heeft
            indicatie_eind_datum = indicatie_datum + timedelta(days=int(random.random()*1500))
        else:
            indicatie_eind_datum = None
        leveringsvorm=random.choice(leveringsvormen)
        indicatie = Indicatie(
            subject=BNode().skolemize(basepath=dummy.Indicatie_), 
            rdf_type=random.choice(indicatie_besluiten),
            client=client,
            start_datum=indicatie_datum,
            eind_datum=indicatie_eind_datum,
            leveringsvorm=leveringsvorm,
            zorgprofiel=random.choice(zorgprofielen),
            indicatie_behandeling = (leveringsvorm == onzzorg.instelling and random.random () <0.5)
        )
        instances.append(indicatie)

        # zorgproces
        locatie=random.choice(Vestigingen)
        zorgproces = ZorgProces(
            subject=BNode().skolemize(basepath=dummy.ZorgProces_),
            locatie=locatie,
            deelnemer=client,
            indicatie=indicatie,
            periode=(indicatie.start_datum, indicatie.eind_datum)
        )
        instances.append(zorgproces)

        # wooneenheid
        if previous_woonenheid and random.random() < 0.1:
            previous_woonenheid.add_occupant(client)
            previous_woonenheid.capaciteit += 1
        else:
            # nieuwe wooneenheid
            wooneenheid = WoonEenheid(
                subject=BNode().skolemize(basepath=dummy.WoonEenheid_),
                locatie=locatie,
                bewoners=[client],
                capaciteit=1
            )
            instances.append(wooneenheid)
            previous_woonenheid = wooneenheid
        
        # lege wooneenheid
        if random.random() < 0.1:
            capaciteit = 1 if random.random() < 0.6 else 2
            lege_wooneenheid = WoonEenheid(
                subject=BNode().skolemize(basepath=dummy.WoonEenheid_),
                locatie=locatie,
                bewoners=[],
                capaciteit=capaciteit
            )
            instances.append(lege_wooneenheid)

    return(instances)

test_instances = generate_clients()
save_pickle_file_from(test_instances, 'generated_test_clients.pkl')
g = create_rdf_from(test_instances)
g.serialize(destination='generated_test_clients.ttl', format='turtle')