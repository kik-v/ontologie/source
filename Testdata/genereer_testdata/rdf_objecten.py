import random
from rdflib import Graph, BNode, Namespace, URIRef, Literal
from rdflib.namespace import XSD, RDF, RDFS, DC
from variables import *
from functions import *
from collections import defaultdict
from dataclasses import dataclass, field
from typing import List, Optional, Union, Tuple
from datetime import date

class Mens():
    def __init__(self):
        werkovereenkomst_lijst = []
        self.subject = BNode().skolemize(basepath=dummy.Human_)
        self.geboortedatum = generate_random_birthdate()
        self.werkovereenkomst_lijst = werkovereenkomst_lijst

    def get_geboortedatum(self):
        return self.geboortedatum

    def get_werkovereenkomst_lijst(self):
        return self.werkovereenkomst_lijst

    def get_subject(self):
        return self.subject

    def append_werkovereenkomst(self, werk_overeenkomst):
        self.werkovereenkomst_lijst.append(werk_overeenkomst)
    
    def to_rdf(self):
        graph = Graph()
        graph.add((self.subject, RDF.type, onzg.Human))
        graph.add((self.subject, onzg.hasDateOfBirth, Literal(self.geboortedatum, datatype=XSD.date)))
        return graph

class WerkOvereenkomst():
    def __init__(self, mens, periode):
        werkafspraken_lijst = []
        verzuim_lijst = []
        gewerkte_periode_lijst = []
        self.subject = BNode().skolemize(basepath=dummy.WerkOvereenkomst_)
        self.werkafspraken_lijst = werkafspraken_lijst
        self.verzuim_lijst = verzuim_lijst
        self.gewerkte_periode_lijst = gewerkte_periode_lijst
        self.rdf_type = random.choice(WerkOvereenkomst_types)
        self.mens = mens
        einddatum = periode[1]
        if self.rdf_type == onzpers.ArbeidsOvereenkomstOnbepaaldeTijd:
            einddatum = False if random.random() < 0.8 else periode[1]
        self.periode = (periode[0], einddatum)

    def get_subject(self):
        return self.subject
    
    def get_periode(self):
        return (self.periode[0], self.periode[1])

    def get_rdftype(self):
        return self.rdf_type
    
    def get_mens(self):
        return self.mens

    def append_werkovereenkomstafspraak(self, werk_overeenkomst_afspraak):
        self.werkafspraken_lijst.append(werk_overeenkomst_afspraak)

    def get_werkafspraken_lijst(self):
        return self.werkafspraken_lijst

    def append_verzuim_periode(self, verzuim_periode):
        self.verzuim_lijst.append(verzuim_periode)

    def get_verzuim_periode_lijst(self):
        return self.verzuim_lijst
    
    def append_gewerkte_periode(self, gewerkte_periode):
        self.gewerkte_periode_lijst.append(gewerkte_periode)

    def get_gewerkte_periode_lijst(self):
        return self.gewerkte_periode_lijst

    def to_rdf(self):
        graph = Graph()
        graph.add((self.subject, RDF.type, self.rdf_type))
        graph.add((self.subject, onzpers.heeftOpdrachtnemer, self.mens.get_subject()))
        graph.add((self.subject, onzg.startDatum, Literal(self.periode[0].isoformat(), datatype=XSD.date)))
        if self.periode[1]:
            graph.add((self.subject, onzg.eindDatum, Literal(self.periode[1].isoformat(), datatype=XSD.date)))
        for werkafspraak in self.werkafspraken_lijst:
            graph.add((self.subject, onzg.hasPart, werkafspraak.get_subject()))
        return graph

class WerkOvereenkomstAfspraak():
    def __init__(self, werkovereenkomst, periode):
        self.subject = BNode().skolemize(basepath=dummy.WerkOvereenkomstAfspraak_)
        self.contractomvang = None
        self.werk_overeenkomst = werkovereenkomst
        self.periode = periode
        self.functie = random.choice(Functies)
        self.werklocatie = random.choice(Vestigingen)
    
    def get_subject(self):
        return self.subject

    def get_functie(self):
        return self.functie

    def get_periode(self):
        return self.periode

    def get_werklocatie(self):
        return self.werklocatie
    
    def add_contractomvang(self, contract_omvang):
        self.contractomvang = contract_omvang

    def get_contractomvang(self):
        return self.contractomvang

    def get_parttime_factor(self):
        eenheid = self.contractomvang.get_eenheid()
        waarde = self.contractomvang.get_waarde()
        if eenheid == onzpers.fte_36:
            return waarde
        elif eenheid == onzpers.Uren_per_week_unit:
            return waarde/36
        else:
            return False

    def to_rdf(self):
        graph = Graph()
        graph.add((self.subject, RDF.type, onzpers.WerkOvereenkomstAfspraak))
        graph.add((self.subject, onzg.isAbout, self.functie))
        graph.add((self.subject, onzg.isAbout, self.werklocatie))
        graph.add((self.subject, onzg.hasPart, self.contractomvang.get_subject()))
        graph.add((self.subject, onzg.startDatum, Literal(self.periode[0].isoformat(), datatype=XSD.date)))
        if self.periode[1]:
            graph.add((self.subject, onzg.eindDatum, Literal(self.periode[1].isoformat(), datatype=XSD.date)))
        return graph

class ContractOmvang():
    def __init__(self):
        self.subject = BNode().skolemize(basepath=dummy.ContractOmvang_)
        self.type = random.choice(['uren', 'parttime'])
        self.eenheid = onzpers.Uren_per_week_unit if self.type == 'uren' else onzpers.fte_36
        self.waarde = random.choice(parttime_factor) if self.type == 'parttime' else random.choice(uren_per_week)
        self.contract_omvang_waarde = BNode().skolemize(basepath=dummy.ContractOmvangWaarde_)
    
    def get_subject(self):
        return self.subject

    def get_eenheid(self):
        return self.eenheid

    def get_waarde(self):
        return self.waarde

    def to_rdf(self):
        graph = Graph()
        graph.add((self.subject, RDF.type, onzpers.ContractOmvang))
        graph.add((self.subject, onzg.isAbout, self.contract_omvang_waarde))
        graph.add((self.contract_omvang_waarde, RDF.type, onzpers.ContractOmvangWaarde))
        graph.add((self.contract_omvang_waarde, onzg.hasDataValue, Literal(self.waarde, datatype=XSD.decimal)))
        graph.add((self.contract_omvang_waarde, onzg.hasUnitOfMeasure, self.eenheid))
        return graph

class GewerktePeriode():
    def __init__(self, werkovereenkomst, dag):
        self.subject = BNode().skolemize(basepath=dummy.GewerktePeriode_)
        self.locatie = random.choice(Vestigingen)
        self.werkovereenkomst = werkovereenkomst
        self.waarde = random.choice([4, 8])
        self.starttime = add_random_time_to_date(dag)
        self.endtime = self.starttime + timedelta(hours=self.waarde)
        self.gewerktetijd = False
        self.deelnemer = werkovereenkomst.get_mens()

    def get_subject(self):
        return self.subject

    def get_locatie(self):
        return self.locatie

    def get_uren(self):
        return self.waarde

    def get_date(self):
        return self.starttime.date()

    def set_gewerktetijd(self, GewerkteTijd):
        self.gewerktetijd = GewerkteTijd

    def get_overeenkomst(self):
        return self.werkovereenkomst

    def get_starttime(self):
        return self.starttime

    def get_dienst(self):
        hour = self.starttime.hour
        if hour >= 7 and hour < 16:
            return 'dag'
        if hour >= 16 and hour <23:
            return 'avond'
        if hour >= 23 or hour < 7:
            return 'nacht'
        return False

    def to_rdf(self):
        graph = Graph()
        graph.add((self.subject, RDF.type, onzpers.GewerktePeriode))
        graph.add((self.subject, onzg.hasParticipant, self.deelnemer.get_subject()))
        graph.add((self.subject, onzg.definedBy, self.werkovereenkomst.get_subject()))
        graph.add((self.subject, onzg.hasPerdurantLocation, self.locatie))
        graph.add((self.subject, onzg.hasBeginTimeStamp, Literal(self.starttime.isoformat(), datatype=XSD.dateTime)))
        graph.add((self.subject, onzg.hasEndTimeStamp, Literal(self.endtime.isoformat(), datatype=XSD.dateTime)))
        graph.add((self.subject, onzg.hasQuality, self.gewerktetijd.get_subject()))
        return graph

class GewerkteTijd():
    def __init__(self, gewerkteperiode):
        self.subject = BNode().skolemize(basepath=dummy.GewerkteTijd_)
        self.waarde = gewerkteperiode.get_uren()

    def get_subject(self):
        return self.subject

    def to_rdf(self):
        graph = Graph()
        durationValue = BNode()
        graph.add((self.subject, RDF.type, onzpers.GewerkteTijd))
        graph.add((self.subject, onzg.hasQualityValue, durationValue))
        graph.add((durationValue, RDF.type, onzg.DurationValue))
        graph.add((durationValue, onzg.hasDataValue, Literal(self.waarde, datatype = XSD.integer)))
        graph.add((durationValue, onzg.hasUnitOfMeasure, onzg.Uur))
        return graph

class VerzuimPeriode():
    def __init__(self, werkovereenkomst, periode, type):
        verzuimtijd_lijst = []
        self.subject = BNode().skolemize(basepath=dummy.VerzuimPeriode_)
        self.werkovereenkomst = werkovereenkomst
        self.deelnemer = werkovereenkomst.get_mens()
        self.periode = periode
        self.verzuimtijd_lijst = verzuimtijd_lijst
        self.type = type

    def get_subject(self):
        return self.subject

    def get_periode(self):
        return self.periode

    def append_verzuimtijd(self, verzuimtijd):
        self.verzuimtijd_lijst.append(verzuimtijd)

    def get_verzuimtijd_lijst(self):
        return self.verzuimtijd_lijst

    def get_type(self): 
        return self.type

    def to_rdf(self):
        graph = Graph()
        graph.add((self.subject, RDF.type, self.type))
        graph.add((self.subject, onzg.hasParticipant, self.deelnemer.get_subject()))
        graph.add((self.subject, onzg.definedBy, self.werkovereenkomst.get_subject()))
        graph.add((self.subject, onzg.startDatum, Literal(self.periode[0], datatype = XSD.date)))
        if self.periode[1]:
            graph.add((self.subject, onzg.eindDatum, Literal(self.periode[1], datatype = XSD.date)))
        for verzuimtijd in self.verzuimtijd_lijst:
            graph.add((self.subject, onzg.hasQuality, verzuimtijd.get_subject()))
        return graph

class VerzuimTijd():
    def __init__(self, verzuim_periode, periode, ziekte_percentage):
        self.subject = BNode().skolemize(basepath=dummy.VerzuimTijd_)
        self.verzuimperiode = verzuim_periode
        self.ziekte_percentage = ziekte_percentage
        self.periode = periode

    def get_subject(self):
        return self.subject

    def get_periode(self):
        return self.periode

    def get_ziekte_percentage(self):
        return self.ziekte_percentage

    def to_rdf(self):
        graph = Graph()
        durationValue = BNode()
        graph.add((self.subject, RDF.type, onzpers.VerzuimTijdKwaliteit))
        graph.add((self.subject, onzg.hasQualityValue, durationValue))
        graph.add((durationValue, RDF.type, onzg.DurationValue))
        graph.add((durationValue, onzg.hasUnitOfMeasure, onzg.percent))
        graph.add((durationValue, onzg.hasDataValue, Literal(self.ziekte_percentage)))
        graph.add((durationValue, onzg.startDatum, Literal(self.periode[0], datatype=XSD.date)))
        if self.periode[1]:
            graph.add((durationValue, onzg.eindDatum, Literal(self.periode[1], datatype=XSD.date)))
        return graph
