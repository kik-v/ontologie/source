from variables import *
from functions import *

def indicator_odb_2_1_1(periode, mensen):
    dagen_totaal = 0
    dagen_periode = days_in_period(periode)
    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        contract_periode = werkovereenkomstafspraak.get_periode()
                        meetperiode = trim_to_period(contract_periode, periode)
                        if meetperiode:
                            dagen_contract += days_in_period(meetperiode)
        dagen_totaal += dagen_contract if dagen_contract < dagen_periode else dagen_periode

    return (f"ODB 2.1.1 indicator:{dagen_totaal/dagen_periode}")

def indicator_odb_2_1_2(periode, mensen):
    gewerkte_uren = 0
    dagen_periode = (periode[1] - periode[0]).days +1
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        ptf = werkovereenkomstafspraak.get_parttime_factor()
                        afspraak_periode = werkovereenkomstafspraak.get_periode()
                        # if period1_inperiod2(afspraak_periode, periode):
                            # (start, eind) = afspraak_periode
                        reken_periode = trim_to_period(afspraak_periode, periode)
                        if reken_periode:
                            for gewerkte_periode in gewerkte_perioden:
                                if date_in_period(gewerkte_periode.get_date(), reken_periode):
                                    gewerkte_uren += gewerkte_periode.get_uren()
                                    # print(werkovereenkomst.get_subject(), reken_periode, gewerkte_periode.get_date(), gewerkte_periode.get_uren())
    return (f"ODB 2.1.2 indicator:{gewerkte_uren/(dagen_periode/365*47)/36}")

def indicator_odb_2_1_3(peildatum, mensen):
    teller = 0
    noemer = 0
    for mens in mensen:
        arbeids_overeenkomst = 0
        bepaalde_tijd = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        afspraak_periode = werkovereenkomstafspraak.get_periode()
                        if date_in_period(peildatum, afspraak_periode):
                            arbeids_overeenkomst = 1
                            if werkovereenkomst.get_rdftype() == onzpers.ArbeidsOvereenkomstBepaaldeTijd:
                                bepaalde_tijd = 1
        if arbeids_overeenkomst == 1:
            noemer += 1
            if bepaalde_tijd == 1:
                teller += 1
    return (f"ODB 2.1.3 teller: {teller}, noemer: {noemer}, indicator:{100 * teller/noemer}")

def indicator_odb_2_2_1(periode, mensen):
    gewerkte_uren = 0
    uren_per_functie = {}
    dagen_periode = (periode[1] - periode[0]).days +1
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                gewerkte_perioden = werkovereenkomst.get_gewerkte_periode_lijst()
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    functie = werkovereenkomstafspraak.get_functie()
                    if functie in ZorgFuncties:
                        functie_niveau = FunctieNiveaus[functie]
                        ptf = werkovereenkomstafspraak.get_parttime_factor()
                        afspraak_periode = werkovereenkomstafspraak.get_periode()
                        reken_periode = trim_to_period(afspraak_periode, periode)
                        if reken_periode:
                            for gewerkte_periode in gewerkte_perioden:
                                uren = gewerkte_periode.get_uren()
                                dag = gewerkte_periode.get_date()
                                if date_in_period(dag, reken_periode):
                                    gewerkte_uren += uren
                                    # print(gewerkte_periode.get_subject(), reken_periode, dag, uren)
                                    if functie_niveau in uren_per_functie:
                                        uren_per_functie[functie_niveau] += uren
                                    else:
                                        uren_per_functie[functie_niveau] = uren
                                    # print(werkovereenkomst.get_subject(), reken_periode, gewerkte_periode.get_date(), gewerkte_periode.get_uren())
    indicator_string = "ODB 2.2.1\n"                                
    for niveau in sorted(uren_per_functie):
        indicator_string += f"           {niveau.split('#')[-1]} teller: {uren_per_functie[niveau]}, noemer: {gewerkte_uren}, indicator: {uren_per_functie[niveau]/gewerkte_uren}\n"
    return (indicator_string)

def indicator_odb_2_3_1(periode, mensen):
    dagen_ziek = 0
    dagen_contract = 0
    for mens in mensen:
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        ptf = werkovereenkomstafspraak.get_parttime_factor()
                        afspraak_periode = werkovereenkomstafspraak.get_periode()
                        if period1_inperiod2(afspraak_periode, periode):
                            (start, eind) = afspraak_periode
                            start_reken = periode[0] if start < periode[0] else start
                            eind_reken = periode[1] if eind > periode[1] or not eind else eind
                            dagen_contract += ptf * ((eind_reken - start_reken).days + 1)
                            reken_periode = (start_reken, eind_reken)
                            for verzuim in werkovereenkomst.get_verzuim_periode_lijst():
                                verzuim_periode = verzuim.get_periode()
                                if period1_inperiod2(verzuim_periode, reken_periode):
                                    for verzuim_tijd in verzuim.get_verzuimtijd_lijst():
                                        verzuim_sub_periode = verzuim_tijd.get_periode()
                                        if period1_inperiod2(verzuim_sub_periode, reken_periode):
                                            ziekte_percentage = verzuim_tijd.get_ziekte_percentage()/100
                                            (start_ziek, eind_ziek) = verzuim_sub_periode
                                            start_ziek_reken = reken_periode[0] if start_ziek < reken_periode[0] else start_ziek
                                            eind_ziek_reken = reken_periode[1] if eind_ziek > reken_periode[1] or not eind_ziek else eind_ziek
                                            dagen_ziek += ptf * ziekte_percentage * ((eind_ziek_reken - start_ziek_reken).days + 1)
                                            # print(f"{verzuim_tijd.get_subject()}, {start_ziek_reken}-{eind_ziek_reken} ptf: {ptf}, zp: {ziekte_percentage}, dagen ziek: {(eind_ziek_reken - start_ziek_reken).days + 1}, totaal: {dagen_ziek}")
    return (f"ODB 2.3.1 teller: {dagen_ziek}, noemer: {dagen_contract}, indicator:{100 * dagen_ziek/dagen_contract}")

def indicator_odb_2_3_2(periode, mensen):
    dagen_totaal = 0
    dagen_periode = days_in_period(periode)
    geldig_verzuim_lijst = set()
    aantal_verzuim = 0
    for mens in mensen:
        dagen_contract = 0
        for werkovereenkomst in mens.get_werkovereenkomst_lijst():
            if werkovereenkomst.get_rdftype() in ArbeidsOvereenkomsten:
                verzuim_lijst = werkovereenkomst.get_verzuim_periode_lijst()
                for werkovereenkomstafspraak in werkovereenkomst.get_werkafspraken_lijst():
                    if werkovereenkomstafspraak.get_functie() in ZorgFuncties:
                        contract_periode = werkovereenkomstafspraak.get_periode()
                        meetperiode = trim_to_period(contract_periode, periode)
                        if meetperiode:
                            dagen_contract += days_in_period(meetperiode)
                            for verzuim in verzuim_lijst:
                                verzuim_periode = verzuim.get_periode()
                                if period1_inperiod2(meetperiode, verzuim_periode):
                                    geldig_verzuim_lijst.add(verzuim)
        dagen_totaal += dagen_contract if dagen_contract < dagen_periode else dagen_periode
    return (f"ODB 2.3.2 teller: {len(geldig_verzuim_lijst)}, noemer:{dagen_totaal/dagen_periode} indicator: {len(geldig_verzuim_lijst)/(dagen_totaal/dagen_periode)}")
