# Validation report results

Found:

* 20 violations

* 1 warnings, and 

* 0 info messages.

## About n2ee655180a2d418d91c997f29212ff48b2

* Something is wrong with the startDatum of this QualificationLevelQuality. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Functie_without_quality_noStartDate_2endDates

* Something is wrong with the eindDatum of this OccupationalPositionRole. 
	* `sh:MaxCountConstraintComponent` violation

* Something is wrong with the quality of this OccupationalPositionRole. 
	* `sh:MinCountConstraintComponent` violation

* Something is wrong with the startDatum of this OccupationalPositionRole. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Omvang_overeenkomst_overlap

* For ContractOmvang http://data.dummyzorg.nl/Omvang_overeenkomst_overlap, the following ContractOmvangWaarden overlap in time: http://data.dummyzorg.nl/omvangwaarde_overlap_badDataValues and http://data.dummyzorg.nl/omvangwaarde_overlap_valid 
	* `sh:SPARQLConstraintComponent` violation

* Something is wrong with the startDatum of this ContractOmvang. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Omvangwaarde_noUnit_noValue

* Something is wrong with the unit of measure of this ContractOmvangWaarde. 
	* `sh:MinCountConstraintComponent` violation

* Something is wrong with the data value of this ContractOmvangWaarde. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Werkovereenkomst_bad

* Something is wrong with the eindDatum of this WerkOvereenkomst. 
	* `sh:DatatypeConstraintComponent` violation / found value: `2021-12-29`

* Something is wrong with the eindDatum of this WerkOvereenkomst. 
	* `sh:MaxCountConstraintComponent` violation

* Something is wrong with the startDatum of this WerkOvereenkomst. 
	* `sh:MinCountConstraintComponent` violation

* Something is wrong with the ContractOmvang of this WerkOvereenkomst. 
	* `sh:QualifiedMinCountConstraintComponent` violation

* Something is wrong with the Opdrachtnemer of this WerkOvereenkomst. 
	* `sh:MaxCountConstraintComponent` violation

## About dummy:Werkovereenkomst_multiple_overlapping_functies

* For overeenkomst http://data.dummyzorg.nl/Werkovereenkomst_multiple_overlapping_functies, the following OccupationalPositionRoles overlap in time: http://data.dummyzorg.nl/Functie_overlap_1 and http://data.dummyzorg.nl/Functie_overlap_2 
	* `sh:SPARQLConstraintComponent` violation

## About dummy:omvang_without_contract

* Something is wrong with the eindDatum of this ContractOmvang. 
	* `sh:DatatypeConstraintComponent` violation / found value: `I am a string`

* This ContractOmvang is not part of any Werkovereenkomst and no Werkovereenkomst has it as part. 
	* `sh:OrConstraintComponent` violation

## About dummy:omvangwaarde_overlap_badDataValues

* Something is wrong with the data value of this ContractOmvangWaarde. 
	* `sh:MaxCountConstraintComponent` violation

* Something is wrong with the data value of this ContractOmvangWaarde. 
	* `sh:OrConstraintComponent` violation / found value: `I am not a number`

## About dummy:qualLevelQual_noOverlap_noStartDate_twoEndDates

* Something is wrong with the startDatum of this QualificationLevelQuality. 
	* `sh:MinCountConstraintComponent` violation

* Something is wrong with the eindDatum of this QualificationLevelQuality. 
	* `sh:MaxCountConstraintComponent` violation

## About dummy:qualLevelQual_noOverlap_twoStartDates

* Something is wrong with the startDatum of this QualificationLevelQuality. 
	* `sh:MaxCountConstraintComponent` violation

