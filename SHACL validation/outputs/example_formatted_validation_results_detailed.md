# Validation report results

Found:

* 20 violations

* 1 warnings, and 

* 0 info messages.

## About n9184850c66c048e2807f1ec7eec48093b2

* This QualificationLevelQuality has no start date. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Functie_without_quality_noStartDate_2endDates

* This OccupationalPositionRole has more than one end dates. 
	* `sh:MaxCountConstraintComponent` violation

* This OccupationalPositionRole has no start date. 
	* `sh:MinCountConstraintComponent` violation

* This OccupationalPositionRole has no (QualificationLevel)Quality. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Omvang_overeenkomst_overlap

* This ContractOmvang has no start date. 
	* `sh:MinCountConstraintComponent` violation

* For ContractOmvang http://data.dummyzorg.nl/Omvang_overeenkomst_overlap, the following ContractOmvangWaarden overlap in time: http://data.dummyzorg.nl/omvangwaarde_overlap_badDataValues and http://data.dummyzorg.nl/omvangwaarde_overlap_valid 
	* `sh:SPARQLConstraintComponent` violation

## About dummy:Omvangwaarde_noUnit_noValue

* This ContractOmvangWaarde has no unit of measure. 
	* `sh:MinCountConstraintComponent` violation

* This ContractOmvangWaarde has no data value. 
	* `sh:MinCountConstraintComponent` violation

## About dummy:Werkovereenkomst_bad

* Something is wrong with the Opdrachtnemer of this WerkOvereenkomst. 
	* `sh:MaxCountConstraintComponent` violation

* This WerkOvereenkomst has more than one end dates. 
	* `sh:MaxCountConstraintComponent` violation

* This WerkOvereenkomst has no start date. 
	* `sh:MinCountConstraintComponent` violation

* This WerkOvereenkomst has no ContractOmvang as part. 
	* `sh:QualifiedMinCountConstraintComponent` violation

* The end date of this WerkOvereenkomst is not of type date. 
	* `sh:DatatypeConstraintComponent` violation / found value: `2021-12-29`

## About dummy:Werkovereenkomst_multiple_overlapping_functies

* For overeenkomst http://data.dummyzorg.nl/Werkovereenkomst_multiple_overlapping_functies, the following OccupationalPositionRoles overlap in time: http://data.dummyzorg.nl/Functie_overlap_1 and http://data.dummyzorg.nl/Functie_overlap_2 
	* `sh:SPARQLConstraintComponent` violation

## About dummy:omvang_without_contract

* This ContractOmvang is not part of any Werkovereenkomst and no Werkovereenkomst has it as part. 
	* `sh:OrConstraintComponent` violation

* The end date of this ContractOmvang is not of type date. 
	* `sh:DatatypeConstraintComponent` violation / found value: `I am a string`

## About dummy:omvangwaarde_overlap_badDataValues

* The data value of this ContractOmvangWaarde is not a decimal or integer. 
	* `sh:OrConstraintComponent` violation / found value: `I am not a number`

* This ContractOmvangWaarde has too many data values. 
	* `sh:MaxCountConstraintComponent` violation

## About dummy:qualLevelQual_noOverlap_noStartDate_twoEndDates

* This QualificationLevelQuality has no start date. 
	* `sh:MinCountConstraintComponent` violation

* This QualificationLevelQuality has more than one end dates. 
	* `sh:MaxCountConstraintComponent` violation

## About dummy:qualLevelQual_noOverlap_twoStartDates

* This QualificationLevelQuality has more than one start dates. 
	* `sh:MaxCountConstraintComponent` violation

