# IMPORTANT NOTICE. ALL FILES AND SHACL SHAPES IN THIS FOLDER ARE COMPATIBLE WITH ONZ ONTOLOGY RELEASE TAGS 2.0.0 AND UP, NOT WITH ONTOLOGY RELEASE TAGS 3.0.0 AND UP!





# Validations Personele samenstelling - Part 1

The following messages indicate the respective issues in the structure of the data in question.
Each of these messages will be shown as a `Violation` of the specified constraints, however note that disabling and adjusting the severity levels and targets of validation are possible. 

## Relationship checks

### QualificationLevelQuality (Kwalificatieniveau)

* startDatum: 
    * `This QualificationLevelQuality has no start date.`
    * `This QualificationLevelQuality has more than one start dates.`
    * `This QualificationLevelQuality has a start date with an invalid data type.`

* eindDatum:
    * `This QualificationLevelQuality has more than one end dates`
    * `This QualificationLevelQuality has an end date with an invalid data type.`

* hasQualityValue:
    * `This QualificationLevelQuality has no EducationalLevelValue, or it is of an incorrect / undefined type.`


### OccupationalPositionRole (Functie)

* hasQuality:
    * `This OccupationalPositionRole has no (QualificationLevel)Quality`
    * `The quality of this OccupationalPositionRole is not a (QualificationLevel)Quality.`

* startDatum:
    * `This OccupationalPositionRole has no start date.`
    * `This OccupationalPositionRole has more than one start dates.`
    * `The start date of this OccupationalPositionRole is not of type date.`

* eindDatum:
    * `This OccupationalPositionRole has more than one end dates.`
    * `The end date of this OccupationalPositionRole is not of type date.`    


### Werkovereenkomst

* startDatum:
    * `This WerkOvereenkomst has no start date.`
    * `This WerkOvereenkomst has too many start dates.`
    * `The start date of this WerkOvereenkomst is not of type date.`

* eindDatum:
    * `This WerkOvereenkomst has more than one end dates.`
    * `The end date of this WerkOvereenkomst is not of type date.`

* hasPart: 
    * `This WerkOvereenkomst has no ContractOmvang as part.`

*  **!** `For overeenkomst [A], the following OccupationalPositionRoles overlap in time: [X] and [Y].`



### ContractOmvang

* startDatum:
    * `This ContractOmvang has no start date.`
    * `This ContractOmvang has too many start dates.`
    * `The start date of this ContractOmvang is not of type date.`

* eindDatum:
    * `This ContractOmvang has too many end dates.`
    * `The end date of this ContractOmvang is not of type date.`

* partOf:
    * `This ContractOmvang is not part of any Werkovereenkomst and no Werkovereenkomst has it as part.`

*  **!** `For ContractOmvang [A], the following ContractOmvangWaarden overlap in time: [X] and [Y].`


### ContractOmvangWaarde

*  hasDataValue:
    * `This ContractOmvangWaarde has no data value.`
    * `This ContractOmvangWaarde has too many data values.`
    * `The data value of this ContractOmvangWaarde is not a decimal or integer.`

*  hasUnitOfMeasure: 
    * `This ContractOmvangWaarde has no unit of measure.`
    * `This ContractOmvangWaarde has too many units of measure.`
    * `The unit of measure of this ContractOmvangWaarde is not a unit of workload.`

* startDatum:
    * `This ContractOmvangWaarde has too many start dates.`
    * `The start date of this ContractOmvangWaarde is not of type date.`

* eindDatum:
    * `This ContractOmvangWaarde has too many end dates.`
    * `The end date of this ContractOmvangWaarde is not of type date.`


### ParttimeFactor  

* hasUnitOfMeasure:
    * `This ParttimeFactor has no unit of measure.`
    * `This ParttimeFactor has more than one units of measure.`
    * `The unit of measure of this ParttimeFactor is not a unit of workload.`

