#!/usr/bin/python3
import rdflib
import os

# The variables and functions in this file are to be used with the accompanying Jupyter notebook
# and have been place here in order to keep it cleaner.


previous_output_files = [
    'outputs' + os.sep + 'ext_graph.ttl',
    'outputs' + os.sep + 'raw_validation_report.ttl',
    'outputs' + os.sep + 'formatted_validation_results.md',
    'outputs' + os.sep + 'formatted_validation_results.html',
    'outputs' + os.sep + 'formatted_validation_results.docx',
]


def print_triples(g) -> None:
  # Prints triples from a graph, also uses the assigned prefixes.
  for triple in g:
    subject, predicate, object = triple[0], triple[1], triple[2]
    #print(f'{g.qname(subject)} {g.qname(predicate)} {g.qname(object)}')
    print_triple = []
    for item in triple:
      if type(item) == rdflib.term.URIRef:
          try:
            print_triple.append(g.qname(item))
          except:
            print_triple.append(str(item))
      else:
         print_triple.append(str(item))
    print('  '.join(print_triple))       

      #g.qname(item) if type(item) == rdflib.term.URIRef else str(item) for item in triple])
  return 


def print_results(results, from_graph) -> None:
    # Prints the results of a query using the prefixes in from_graph
    for row in results:
       print([from_graph.qname(item) if type(item) == rdflib.term.URIRef else str(item) for item in row ])
    return


def bind_common_namespaces(g=rdflib.Graph()) -> rdflib.Graph:
  g.bind('onz-g', rdflib.Namespace('http://purl.org/ozo/onz-g#'))
  g.bind('onz-pers', rdflib.Namespace('http://purl.org/ozo/onz-pers#')) 
  g.bind('onz-fin', rdflib.Namespace('http://purl.org/ozo/onz-fin#')) 
  g.bind('onz-zorg', rdflib.Namespace('http://purl.org/ozo/onz-zorg#')) 
  g.bind('onz-org', rdflib.Namespace('http://purl.org/ozo/onz-org#')) 
  g.bind('onz-ph', rdflib.Namespace('http://purl.org/ozo/onz-ph#')) 
  g.bind('onz-query', rdflib.Namespace('http://placeholder.org/data/query#'))
  g.bind('dcterms', rdflib.Namespace('http://purl.org/dc/terms/')) 
  g.bind('sh', rdflib.Namespace('http://www.w3.org/ns/shacl#'))
  return g


def count_validation_outputs(report_graph) -> dict:
    # Counts the number of violations, warnings and info in a SHACL report graph
    validation_output_count_query = '''
        SELECT ?severity (count(?vr) as ?severityCount) 
        WHERE {
            ?vr sh:resultSeverity ?severity .
        } GROUP BY ?severity
    '''
    validation_count_results = report_graph.query(validation_output_count_query)

    results_as_dict = {'Violation': 0, 'Warning': 0, 'Info': 0}
    for (level, levelCount) in validation_count_results:
        levelPart = str(level).split('#')[1]
        results_as_dict[levelPart] = str(levelCount)

    return results_as_dict


def report_to_markdown(report_graph, output_filename, data_graph) -> str:
    # Generates a markdown file from a validation report, also uses prefixes assigned in the data_graph
    validation_report_query = '''
    SELECT ?focusNode ?message ?violatedComponent ?val
    WHERE {
        ?vr a sh:ValidationResult ;
            sh:focusNode ?focusNode ;
            sh:resultMessage ?message ;
            sh:sourceConstraintComponent ?violatedComponent .
        OPTIONAL { ?vr sh:value ?val . }
    } order by ?focusNode
    '''

    validation_query_results = report_graph.query(validation_report_query)
    #print_results(validation_query_results, combined_graph)

    # group the reports per node
    results_per_node = {}
    for item in list(validation_query_results):
        focusNode, message, violatedComponent, val = item
        # print(focusNode, message, violatedComponent)
        if focusNode not in results_per_node:
            results_per_node[focusNode] = list()
        report_form = {'message': message, 'violated component': data_graph.qname(violatedComponent)}
        pretty_focusNode = data_graph.qname(focusNode) if type(focusNode) == rdflib.term.URIRef else str(focusNode)

        if val:
          pretty_val = data_graph.qname(val) if type(val) == rdflib.term.URIRef else str(val)
          if val != focusNode:
            report_form['value'] = pretty_val
       
        results_per_node[focusNode].append(report_form)

    total_str = '# Validation report results\n\n'
    output_counts = count_validation_outputs(report_graph)
    total_str += f'Found:\n\n* {output_counts["Violation"]} violations'
    total_str += f'\n\n* {output_counts["Warning"]} warnings, and \n\n* {output_counts["Info"]} info messages.\n\n'

    for node in results_per_node:
        pretty_focusNode = data_graph.qname(node) if type(node) == rdflib.term.URIRef else str(node)
        total_str += f'## About {pretty_focusNode}\n\n'
        for entry in results_per_node[node]:
            message = entry['message']
            violation = entry['violated component']
            total_str += f'* {message} \n\t* `{violation}` violation'
            if 'value' in entry:
               reported_value = entry['value']
               total_str += f' / found value: `{reported_value}`'
            total_str += '\n\n'
            
    with open(output_filename, 'w') as md_out:
        md_out.write(total_str)

    return total_str


def fetch_ontologies(onto_list=None, use_remote=False, onto_folder='..') -> rdflib.Graph:
    # Reads in and returns the ontologies from a local folder or their Gitlab locations.
    ontology_paths = dict()
    if use_remote:
        ontology_paths = {
            'onz-g': 'https://gitlab.com/kik-v/ontologie/source/-/raw/development/onz-g.owl',
            'onz-fin': 'https://gitlab.com/kik-v/ontologie/source/-/raw/development/onz-fin.owl',
            'onz-org': 'https://gitlab.com/kik-v/ontologie/source/-/raw/development/onz-org.owl',
            'onz-pers': 'https://gitlab.com/kik-v/ontologie/source/-/raw/development/onz-pers.owl',
            'onz-zorg': 'https://gitlab.com/kik-v/ontologie/source/-/raw/development/onz-zorg.owl',
        }    
    else:  
        ontology_paths = {
            'onz-g': onto_folder + os.sep + 'onz-g.owl',
            'onz-fin': onto_folder + os.sep + 'onz-fin.owl',
            'onz-org': onto_folder + os.sep + 'onz-org.owl',
            'onz-pers': onto_folder + os.sep + 'onz-pers.owl',
            'onz-zorg': onto_folder + os.sep + 'onz-zorg.owl'
        }

    ontology_graph = rdflib.Graph()
    for onto in onto_list:
        if onto in ontology_paths:
            ontology_graph += rdflib.Graph().parse(ontology_paths[onto], format='xml')
        else:
            print(f'Ontology {onto} source not defined.')
    
    return ontology_graph


def clean_up_files(filepath_list=previous_output_files) -> None:
    # Cleans up files, assuming they exist.
    for file in filepath_list:
        if os.path.exists(file): 
            print(f'Deleting {file} file from previous run.')
            os.remove(file)
    return