# De ONZ Ontologieën

Deze repository bevat de bronbestanden van de ONZ-ontologieën en diverse andere bestanden 
die het gebruik van de ontologieën en het testen van queries ondersteunen.

Alle bestanden in deze repository zijn ontwikkeld binnen context van het KIK-V programma (2019-) 
geleid door het Zorginstituut Nederland. Doel van dit programma is de uniforme gegevensuitwisseling 
over bedrijfsvoeringsinformatie binnen de verpleeghuiszorg in Nederland. Zie: [Programma KIK-V](https://www.kik-v.nl/).



Deze repository bevat de volgende bestanden:

## Ontologieën en bijbehorende datafiles

* `onz-g.owl`   
De onz-g is de generieke ontologie die functioneert als top- en core-ontologie voor vier verschillende domeinontologieën: onz-pers, onz-org, onz-zorg en onz-fin.
* `onz-pers.owl`   
De onz-pers is specifiek ontwikkeld voor het typeren van en queryen over brondata die in verpleeghuisorganisaties worden vastgelegd over personele samenstelling, opleidingsniveau, arbeidscontracten etc.
* `onz-org.owl`  
De onz-org is specifiek ontwikkeld voor het typeren van en queryen over brondata die in verpleeghuisorganisaties worden vastgelegd over vestigingen, afdelingen, adresgegevens, KvK-inschrijvingen, etcetera.
* `onz-zorg.owl`  
De onz-zorg is specifiek ontwikkeld voor het typeren van en queryen over brondata die in verpleeghuisorganisaties worden vastgelegd over bijvoorbeeld zorgprofielen, indicaties en zorgprocessen.
* `onz-fin.owl`
De onz-fin is specifiek ontwikkeld voor het typeren van en queryen over brondata die in verpleeghuisorganisaties worden vastgelegd over bijvoorbeeld zorgdeclaraties, inkomsten en uitgaven.


Alle domeinontologieën importeren de generieke onz-g ontologie via een owl:imports statement. Verdere informatie over versionering, licenties, updates en hergebruikte vocabularies is te vinden in de metadata van de ontologieën zelf.

In de map `Ontologie_data` zijn rdf databestanden opgenomen die bij de onz ontologieën horen, maar apart beheerd worden vanwege een andere wijzigings- en beheercyclus. In deze map staat onder andere een RDF-versie van het Prismant Referentie Grootboekschema. 
Meer informatie over de bestanden is te vinden in de readme aldaar

## Platen en Visualisaties

In deze map zijn visualisaties te vinden van de domeinontologieën en van enkele design patterns in de generieke onz-g.

## Doc

In deze map is gedetailleerde documentatie te vinden over onz-pers.

## Scripts

Deze map bevat de Pythonscripts om de ontologiedatabestanden te kunnen genereren, zie `Ontologie_data`. Verder is er de subfolder te vinden voor het genereren van testdata voor queries gebaseerd op de ONZ ontologieën; 
uitgebreide informatie is te vinden in de readme in dat bestand.

## SHACL validatie

In deze map zijn de SHACL shapes vinden voor het valideren van data over personele samenstelling.


## Testbestanden Ontologieën

De map `ONZ-G consistency test files` bevat de testbestanden voor de ontologieën. Informatie over de werkwijze en bijbehorende bestanden is te vinden in de bijbehorende readme.


## Testdata voor SPARQL queries

In de map `Testdata` zijn de oude, statische testbestanden te vinden om de queries te testen.
