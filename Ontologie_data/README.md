# Ontologie data
In deze map staan rdf bestanden waarin data opgenomen is die wel bij de onz ontologieën horen, maar daar om praktische redenen geen onderdeel van zijn. Meest voor de hand liggende reden is de mogelijkheid om deze data te kunnen wijzigen, zonder dat daarvoor een nieuwe versie van een ontologie voor hoeft te worden gepubliceerd.
## Voorbeelden
- triples met daarin instanties voor iedere dag van de diverse meetperiodes, voorzien van een numerieke positie. Hierdoor is het mogelijk het verschil tussen twee dagen exact te berekenen zonder speciale algoritmes, welke ontbreken in de SPARQL 1.1 standaard.
- RGS grootboekschema;
- Prismant Referentie grootboekschema;
- Nederlandse woonplaatsen en gemeentes inclusief identifiers van het CBS en URI's uit BAG en TOOI;
- Nederlandse zorgkantoorregio's en de gemeenten die tot een bepaalde zorgkantoorregio behoren.